﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarcarCasillas : MonoBehaviour {

    private ControladorJuego controladorJuego;
    private ControlSelector controlSelector;
    private int[,] movimientosPermitidos;
    private int anchoMapa, altoMapa;

    private GameObject movCasilla; //GameObject que marcará el camino que recorrerá el personaje.
    private GameObject atkCasilla; //Marcará las casillas a las que podrá atacar.

    private void Awake() {
        movCasilla = (GameObject)Resources.Load("Prefabs/MovCasilla", typeof(GameObject));
        atkCasilla = (GameObject)Resources.Load("Prefabs/CasillaAtk", typeof(GameObject));
        controlSelector = GameObject.FindWithTag("Selector").GetComponent<ControlSelector>();
    }

    private void Start() {
        controladorJuego = ControladorJuego.GetInstancia();
        anchoMapa = controladorJuego.ancho;
        altoMapa = controladorJuego.alto;
    }

    //Con este método marcamos en el tablero las posiciones donde podemos mover el personaje.
    public void MarcarMovsDisponibles(int movs, Personaje pj) {
        movimientosPermitidos = new int[anchoMapa, altoMapa]; //Reseteamos

        movimientosPermitidos[(int)pj.transform.position.x, (int)pj.transform.position.y] = 1;

        //Diagonal Arriba-D.

        for (int y = 0; y <= movs; y++) {
            for (int x = 0; x <= movs - y; x++) {
                Vector2 a = new Vector2(Mathf.Clamp(pj.transform.position.x + x, 0f, anchoMapa - 1), Mathf.Clamp(pj.transform.position.y + y, 0f, altoMapa - 1));
                Marcar(a);
            }
        }

        //Diagonal Abajo-I

        for (int y = 0; y <= movs; y++) { //for (int y = movs; y >= 0; y--) {
            for (int x = 0; x <= movs - y; x++) {//for (int x = movs - y; x >= 0; x--) {
                Vector2 a = new Vector2(Mathf.Clamp(pj.transform.position.x - x, 0f, anchoMapa - 1), Mathf.Clamp(pj.transform.position.y - y, 0f, altoMapa - 1));
                Marcar(a);
            }

        }

        //Diagonal Arriba-I
        //Ahora es y=1 x=1 para evitar volver a pasar por la misma posición que en los dos anteriores.
        for(int y = 1; y <= movs; y++) {
            for (int x = 1; x <= movs - y; x++) {
                Vector2 a = new Vector2(Mathf.Clamp(pj.transform.position.x - x, 0f, anchoMapa - 1), Mathf.Clamp(pj.transform.position.y + y, 0f, altoMapa - 1));
                Marcar(a);
            }

        }

        //Diagonal Abajo-D

        for (int y = 1; y <= movs; y++) {
            for (int x = 1; x <= movs - y; x++) {
                Vector2 a = new Vector2(Mathf.Clamp(pj.transform.position.x + x, 0f, anchoMapa - 1), Mathf.Clamp(pj.transform.position.y - y, 0f, altoMapa - 1));
                Marcar(a);
            }
        }

    }

    /*public bool HayPiedra(Vector2 a) {
        if (controladorJuego.mapaPersonajes[(int)a.x, (int)a.y] != null && controladorJuego.mapaPersonajes[(int)a.x, (int)a.y].CompareTag("Intocable")) {
            return true;
        }
        return false;

    }*/

    public void Marcar(Vector2 a) {
        if (movimientosPermitidos[(int)a.x, (int)a.y] != 1 && controladorJuego.mapaPersonajes[(int)a.x, (int)a.y] == null) {
            movimientosPermitidos[(int)a.x, (int)a.y] = 1;
            Instantiate(movCasilla, new Vector3((int)a.x, (int)a.y), Quaternion.identity);
        }
    }

    private void MarcarAtk(Vector3 posicion, Personaje pj) {

        posicion = new Vector3(Mathf.Clamp(posicion.x, 0f, anchoMapa - 1), Mathf.Clamp(posicion.y, 0f, altoMapa - 1), posicion.z);

        if (controladorJuego.mapaPersonajes[(int)posicion.x, (int)posicion.y] != null) {//Si hay un personaje en esta casilla.
            if (!controladorJuego.mapaPersonajes[(int)posicion.x, (int)posicion.y].CompareTag(pj.tag) && !controladorJuego.mapaPersonajes[(int)posicion.x, (int)posicion.y].CompareTag("Intocable")) {//Si el personaje NO es aliado ni un obstáculo.
                movimientosPermitidos[(int)posicion.x, (int)posicion.y] = 1;
                Instantiate(atkCasilla, posicion, Quaternion.identity);
            }
        } else {
            Instantiate(atkCasilla, posicion, Quaternion.identity);
        }
    }

    public void AtkDisponibles(Personaje pj) {

        movimientosPermitidos = new int[anchoMapa, altoMapa]; //Reseteamos

        MarcarAtk(new Vector3(pj.transform.position.x + 1, pj.transform.position.y, -0.1f), pj);
        MarcarAtk(new Vector3(pj.transform.position.x - 1, pj.transform.position.y, - 0.1f), pj);
        MarcarAtk(new Vector3(pj.transform.position.x, pj.transform.position.y + 1, - 0.1f), pj);
        MarcarAtk(new Vector3(pj.transform.position.x, pj.transform.position.y - 1, - 0.1f), pj);

    }

    public bool GetDisp() {
        if (movimientosPermitidos[(int)controlSelector.transform.position.x, (int)controlSelector.transform.position.y] == 1) {
            return true;
        }
        return false;
    }

    public void LimpiarTablero() {
        //Busco todos los objetos con el nombre 'SeleccionMovimiento' y los guardo en un array.
        GameObject[] objetos = GameObject.FindGameObjectsWithTag("SeleccionMovimiento");
        //Recorro el array destruyendo todos los objetos, dejando limpio el tablero.
        foreach (GameObject obj in objetos) {
            Destroy(obj);
        }
    }

}
