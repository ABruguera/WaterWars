﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EstadoJuegoMultijugador : MonoBehaviour {

    public static EstadoJuegoMultijugador estadoJuegoMultijugador;
    public static ControlSelectorMultijugador controlSelectorMultijugador;
    public BaseDatos baseDatos;
    private Socket servidorUsuarios;
    private Socket administradorDePartidas;
    private string HOST = "192.168.12.153";
    private int PUERTO = 6666;
    private string usuario;
    public string nombreJugador1, nombreJugador2;
    private int equipo;
    private bool esJugador1;

    // Se ejecuta antes que el método "Start".
    void Awake()
    {
        if (estadoJuegoMultijugador == null){
            estadoJuegoMultijugador = this;
            DontDestroyOnLoad(gameObject);  // Hace que el objeto no se destruya al cargar otra escena.
        }
        else if (estadoJuegoMultijugador != this)
        {
            Destroy(gameObject);
        }

    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        /*if (SceneManager.GetActiveScene().Equals(SceneManager.GetSceneByName("Prueba_tableroMultijugador")) && !baseDatosInicializada)
        {
            baseDatosInicializada = true;
            baseDatos = FindObjectOfType<BaseDatos>();
            //baseDatos.InicializarEquipos();
        }*/
    }

    // Método para asignar el equipo por defecto leído desde el servidor.
    // PUEDE QUE ESTE MÉTODO SE BORRE.
    public void CargarDatosJugador()
    {
        byte[] bytes = new byte[1024];
        byte[] msg = Encoding.ASCII.GetBytes("2\n");  // Pide al servidor todos los datos del jugador.
        servidorUsuarios.Send(msg);
        int bytesRec = servidorUsuarios.Receive(bytes);  // Recibe el mensaje del servidor en forma de bytes.
        string[] datos = Encoding.ASCII.GetString(bytes, 0, bytesRec).Split(':');
        //usuario = datos[0];
        equipo = int.Parse(datos[1]);
    }

    // Método que retorna el equipo del jugador que carga por defecto del servidor.
    public int GetEquipoPorDefecto()
    {
        return equipo;
    }

    // Método para guardar la conexión establecida con el servidor de usuarios en este script.
    public void SetServidorUsuarios(Socket servidorUsuarios)
    {
        this.servidorUsuarios = servidorUsuarios;
    }

    // Método para retornar el nombre de usuario el cual ha iniciado sesión.
    public string GetUsuarioSalaMultijugador()
    {
        return usuario;
    }

    // Método para asignar el nombre de usuario el cual ha iniciado sesión.
    public void SetUsuarioSalaMultijugador(string usuario)
    {
        this.usuario = usuario;
    }

    // Método para cambiar el equipo del jugador.
    public void CambiarEquipo(string equipo)
    {
        byte[] msg = Encoding.ASCII.GetBytes("1:" + equipo + "\n");
        servidorUsuarios.Send(msg);
        print("Cambiar equipo en: " + servidorUsuarios.RemoteEndPoint.ToString());
    }

    // Método para iniciar la partida online.
    public void IniciarPartida()
    {
        try
        {
            administradorDePartidas = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            administradorDePartidas.Connect(HOST, PUERTO);
            print("Socket connected to " + administradorDePartidas.RemoteEndPoint.ToString());
        }
        catch (SocketException se)
        {
            print("SocketException : " + se.ToString());
        }
    }

    // Método para finalizar la conexión con el servidor de usuarios.
    public void CerrarConexion()
    {
        byte[] msg = Encoding.ASCII.GetBytes("0\n");
        servidorUsuarios.Send(msg);  // Manda la opcion para finalizar la sessión.
        servidorUsuarios.Close();
    }

    // Método para coger el nombre de los usuarios.
    public void GetUsuarios()
    {
        byte[] bytes = new byte[1024];
        int bytesRec = administradorDePartidas.Receive(bytes);
        string[] resultado = Encoding.ASCII.GetString(bytes, 0, bytesRec).Split('\n');
        nombreJugador1 = resultado[0];
        nombreJugador2 = resultado[1];
        if (nombreJugador1.Equals(usuario))
        {
            esJugador1 = true;
        } else
        {
            esJugador1 = false;
        }
        //GameObject.FindGameObjectWithTag("GameController").GetComponent<Interfaz>().Lala();
    }

    // Método para enviar una notificación al servidor para que continue con la ejecución de la partida.
    public void Notificar()
    {
        byte[] msg = Encoding.ASCII.GetBytes("0\n");
        administradorDePartidas.Send(msg);
    }

    //
    public void CargarJugadores() {
        byte[] bytes = new byte[1024];
        int bytesRec = administradorDePartidas.Receive(bytes);
        string resultado = Encoding.ASCII.GetString(bytes, 0, bytesRec);
        string[] equipos = resultado.Split(':');
        print(equipos[0] + "      " + equipos[1]);
        baseDatos.CargarImagenesEquipos(int.Parse(equipos[0]), int.Parse(equipos[1]));

    }

    //
    public bool GetEsJugador1()
    {
        return esJugador1;
    }

    public Socket GetSocketPartida() {
        return administradorDePartidas; 
    }

}
