﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Batalla : MonoBehaviour {
    
    private EstadoJuego estadoJuego;
    public Transform transformAgresor;
    public Transform transformVictima;
    private Personaje agresor;
    private Personaje victima;
    private float tiempoEspera;
    private ControlSelector controlSelector;
    private GameObject atacante, atacado;
    private int danyo;
    public Text danyoEfectuado;
    private string mensajeTurno;
    private bool danyoCritico;
    private BaseDatos baseDatos;
    private float tiempoEsperaVictima;

    private void Awake()
    {
        estadoJuego = FindObjectOfType<EstadoJuego>();
        controlSelector = FindObjectOfType<ControlSelector>();
        baseDatos = FindObjectOfType<BaseDatos>();
        danyoEfectuado.color = new Color(255, 0, 0, 0);
    }

    // Use this for initialization
    void Start()
    {
        mensajeTurno = "";
        danyoEfectuado.text = "";
        danyoCritico = false;
        atacante = null;
        agresor = null;
        victima = null;
        IniciarCombate();
    }

    // Método para iniciar el combate.
    public void IniciarCombate()
    {
        InicializarCombatientes();
        EjecutarAtaque();
    }

    // Método para coger a los combatientes del Estado Juego y visualizarlos.
    private void InicializarCombatientes()
    {
        // Recoge los scripts de los combatientes.
        agresor = estadoJuego.GetAgresor();
        victima = estadoJuego.GetVictima();

        // Visualiza el sprite de la victima, ya que el de el agresor será un GameObject que se instanciará en el siguiente método.
        //spriteRenderVictima.sprite = victima.GetImagen();
    }

    // Método para ejecutar la animación del agresor.
    private void EjecutarAtaque()
    {
        // Carga el prefab del personaje que va a atacar y lo instancia.
        GameObject objeto = (GameObject)Resources.Load("Prefabs/Personajes/" + agresor.GetNombrePrefab(), typeof(GameObject));
        atacante = Instantiate(objeto, transformAgresor.position, Quaternion.identity);
        atacante.GetComponent<Animator>().SetBool("Atacando", true);
        atacante.GetComponent<Animator>().enabled = true;
        agresor.ReproducirSonidoAgresor();

        // Carga el prefab del personaje que va a ser atacado, lo instancia y le quita la animación.
        objeto = (GameObject)Resources.Load("Prefabs/Personajes/" + victima.GetNombrePrefab() + "_Victima", typeof(GameObject));
        atacado = Instantiate(objeto, transformVictima.position, Quaternion.identity);
        atacado.GetComponent<Animator>().SetBool("Atacando", false);
        atacado.GetComponent<Animator>().enabled = true;
        tiempoEsperaVictima = atacante.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length;
        //StartCoroutine(EsperarSonidoVictima());  // Ejecuta la corrutina para el sonido de la víctima.
        StartCoroutine(PararAnimacionVictima());
        // NOTA: Se puede hacer una segunda animación (que sería la que reciba el golpe) y en caso de que éste contraataque pues la animación se cambiaría y atacaría él.

        // Rota al prefab del jugador que va a atacar para que esté mirando a su rival.
        Vector3 newScale = atacante.transform.localScale;
        newScale.x *= -1;
        atacante.transform.localScale = newScale;

        // Almacena la longitud de la animación del prefab e inicia la corrutina de espera;
        tiempoEspera = atacante.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length;

        //danyo = agresor.GetDanyo();
        //victima.aplicarDanyo(danyo);
        AplicarAtaque();


        controlSelector.estadoCursor = 0;

        StartCoroutine(Esperar());
    }

    //
    IEnumerator PararAnimacionVictima()
    {
        yield return new WaitForSeconds(tiempoEsperaVictima);
        atacado.GetComponent<Animator>().enabled = false;
    }

    // Método para aplicar el ataque del agresor a la víctima.
    private void AplicarAtaque()
    {
        danyo = agresor.GetDanyo();
        int estadoAlterado = -1;

        //Si no está cegado ni confuso.
        if (agresor.GetEstadoAlterado()[0] != 1 && agresor.GetEstadoAlterado()[1] != 1 && agresor.GetEstadoAlterado()[0] != 5 && agresor.GetEstadoAlterado()[1] != 5) {
            int suerteAgresor = agresor.GetSuerte();
            int suerteVictima = victima.GetSuerte();

            int aleatorioAgresor = Random.Range(1, 100);
            int aleatorioVictima = Random.Range(1, 100);

            if (aleatorioAgresor <= suerteAgresor)
            {
                danyoCritico = true;
                danyo = danyo * 2;  // Asigna el daño crítico.
            }

            aleatorioAgresor = Random.Range(1, 100);  // Vuelve a calcular el número aleatorio para la habilidad especial.

            // En caso de que el personaje ejecute la habilidad especial:
            if (aleatorioAgresor <= suerteAgresor) estadoAlterado = (byte)agresor.GetHabilidad();

            if (estadoAlterado == 4) danyo += victima.GetVida();  // En caso de que sea el sireno hará tanto daño como vida tenga la víctima.

            if (estadoAlterado != -1)
            {
                string nombreHabilidad = baseDatos.ObtenerNombreHabilidad(estadoAlterado);
                mensajeTurno = nombreHabilidad + " applied!\nDamage received: ";  // En caso de aplicar un estado, se guarda el mensaje para poder visualizarlo al finalizar el turno.
            }

            if (aleatorioVictima <= suerteVictima)
            {
                // En caso de que la víctima esquive el ataque.
                danyo = 0;
                estadoAlterado = -1;
                mensajeTurno = "Dodged attack!\nDamage received: ";
            }

            victima.AplicarDanyo(danyo, estadoAlterado);

        } else{
            if(agresor.GetEstadoAlterado()[0] == 1 || agresor.GetEstadoAlterado()[1] == 1) {
                danyo = 0;  // En caso de que el agresor esté ciego no hará daño.
                mensajeTurno = "Failed attack!\nDamage received: ";
            } else if (agresor.GetEstadoAlterado()[0] == 5 || agresor.GetEstadoAlterado()[1] == 5) {
                //Si está confuso, tendrá un 40% de posibilidades de atacarse a sí mismo. Si consigue acertar al rival, no le aplicará ni daño crítico ni habilidad.
                if (Random.Range(1, 101) <= 40) {
                    agresor.AplicarDanyo(agresor.GetDanyo(), 0);
                    mensajeTurno = "It hurt itself in its confusion!\n";
                    danyoEfectuado = GameObject.FindGameObjectWithTag("TxtAgresor").GetComponent<Text>();
                } else {
                    victima.AplicarDanyo(danyo, -1);
                }
            }
            
        }
  
    }

    // Corrutina para esperar a que se ejecute el turno para poder finalizar el combate.
    IEnumerator Esperar()
    {
        // Espera a que se ejecute toda la animación, aplica el daño a la victima finaliza la batalla.
        yield return new WaitForSeconds(tiempoEspera);
        
        victima.ComprobarEstadoPersonaje();  // Comprueba que la víctima esté muerta o no.

        // Desactiva la animación de atacante para mostrar el daño que éste ha producido a la víctima.
        atacante.GetComponent<Animator>().enabled = false;

        //Si el atacante está confuso y ha recibido daño, ha de cambiar el Text de posición.
        /*if (mensajeTurno.Contains("confuso")) {
            danyoEfectuado.transform.position = new Vector2(agresor.transform.position.x, transform.position.y);
        }*/
        danyoEfectuado.text = mensajeTurno + danyo.ToString();  // Se le asigna el mensaje y el daño al texto del canvas.

        if (danyoCritico && danyo != 0) danyoEfectuado.text += "\nCRITIC DAMAGE!";
        Transform texto = danyoEfectuado.GetComponent<Transform>();

        // Se hace un for aumentando la posición de la Y del texto para que se desplace hacía arriba.
        for (float i = 0; i < 5.2f; i = i + 0.1f)
        {
            if (danyoEfectuado.color.a != 255)
            {
                // Para controlar la transparencia.
                danyoEfectuado.color = new Color(255, 0, 0, danyoEfectuado.color.a + 1f);
            }
            texto.position = new Vector2(texto.position.x, texto.position.y + i);
            yield return null;
        }

        yield return new WaitForSeconds(1f);  // Esperamos 1 segundo para que los jugadores puedan apreciar bien el daño que le ha producido.

        agresor.GetComponent<MoverPersonaje>().atacando = false;
        agresor.GetComponent<MoverPersonaje>().enabled = false;
        GameObject.Destroy(atacante);
        GameObject.Destroy(atacado);
        SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Batalla_" + SceneManager.GetActiveScene().name));
    }

    IEnumerator EsperarSonidoVictima()
    {
        yield return new WaitForSeconds(1f);
        victima.ReproducirSonidoVictima();
    }
}
