using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pruebas_ControladorGamePads : MonoBehaviour {

    private void Start() {

        print("Joystics conectados: ");

        foreach(string s in Input.GetJoystickNames()) {
            print(s);
        }

    }

    private void Update() {
        if (Input.GetAxis("J1-FlechasArribaAbajo") == 1) {
            print("J1 - Hola, soy la flecha de arriba :D :D :D");
        }
        if (Input.GetAxis("J1-FlechasArribaAbajo") == -1) {
            print("J1 - Hola, soy la flecha de abajo :D :D :D");
        }

        if (Input.GetAxis("J1-FlechasDerechaIzquierda") == 1) {
            print("J1 - Hola, soy la flecha de la derecha :D :D :D");
        }
        if (Input.GetAxis("J1-FlechasDerechaIzquierda") == -1) {
            print("J1 - Hola, soy la flecha de la izquierda :D :D :D");
        }

        if (Input.GetAxis("J1-Start") == 1) {
            print("J1 - Hola, soy el start :D :D :D");
        }
        if (Input.GetAxis("J1-O/B") == 1) {
            print("J1 - Hola, soy el Círculo/B :D :D :D");
        }
        if (Input.GetAxis("J1-X/A") == 1) {
            print("J1 - Hola, soy la Equis/A :D :D :D");
        }

        if (Input.GetAxis("J2-FlechasArribaAbajo") == 1) {
            print("J2 - Hola, soy la flecha de arriba :D :D :D");
        }
        if (Input.GetAxis("J2-FlechasArribaAbajo") == -1) {
            print("J2 - Hola, soy la flecha de abajo :D :D :D");
        }

        if (Input.GetAxis("J2-FlechasDerechaIzquierda") == 1) {
            print("J2 - Hola, soy la flecha de la derecha :D :D :D");
        }
        if (Input.GetAxis("J2-FlechasDerechaIzquierda") == -1) {
            print("J2 - Hola, soy la flecha de la izquierda :D :D :D");
        }

        if (Input.GetAxis("J2-Start") == 1) {
            print("J2 - Hola, soy el start :D :D :D");
        }
        if (Input.GetAxis("J2-O/B") == 1) {
            print("J2 - Hola, soy el Círculo/B :D :D :D");
        }
        if (Input.GetAxis("J2-X/A") == 1) {
            print("J2 - Hola, soy la Equis/A :D :D :D");
        }

    }

}
