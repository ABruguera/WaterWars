﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.SqliteClient;
using UnityEngine.UI;
using System;

//ToDo: 
//  Barajar miembros de equipo para que cada vez empiecen en diferente posición.
//  Pillar también la imagen de cada personaje.

public class BaseDatos : MonoBehaviour {

    private string _constr;
    private IDbConnection _dbc;
    private IDbCommand _dbcm;
    private IDataReader _dbr;
    private string consulta = "SELECT nombre,movimientos,danyo,vida,nombreImagen,suerte,id_equipo FROM personajes WHERE nombreImagen = ";

    public GameObject equipo1, equipo2;
    GameObject[] equipoTemp = null;

    private EstadoJuego estadoJuego;
    private EstadoJuegoMultijugador estadoJuegoMultijugador;

    private string A_jefe, A_esbirros1, A_esbirros2;
    private string B_jefe, B_esbirros1, B_esbirros2;


    private void Awake() {
        _constr = "URI=file:" + Application.streamingAssetsPath + "/DDBB/BaseDatos.db";
        _dbc = new SqliteConnection(_constr);
        _dbc.Open();

        _dbcm = _dbc.CreateCommand();

        if (equipo1 != null) {
            estadoJuego = FindObjectOfType<EstadoJuego>();

            estadoJuegoMultijugador = FindObjectOfType<EstadoJuegoMultijugador>();
            if (estadoJuegoMultijugador == null)
            {
                CargarImagenesEquipoA(estadoJuego.GetEquipo1());
                CargarImagenesEquipoB(estadoJuego.GetEquipo2());

                InicializarEquipos();
            }
            
        }
        // ObtenerEquipo se ha desplazado a un método llamado "InicializarEquipos", porque sino en la escena de Seleccionar Equipo daba error.
    }

    //
    /*private void Start()
    {
        estadoJuego = FindObjectOfType<EstadoJuego>();
    }*/

    //
    public void CargarImagenesEquipos(int jugador1, int jugador2)
    {
        CargarImagenesEquipoA(jugador1);
        CargarImagenesEquipoB(jugador2);

        InicializarEquipos();
    }

    // Métodod para obtener los equipos.
    public void InicializarEquipos()
    {
        ObtenerEquipo(1);
        ObtenerEquipo(2);
    }

    private void RellenarDatos(int i) {
        AudioClip[] sonidos = new AudioClip[3];
        sonidos[0] = (AudioClip)Resources.Load("Audios/Personajes/" + _dbr.GetString(4) + "_seleccionado", typeof(AudioClip));
        sonidos[1] = (AudioClip)Resources.Load("Audios/Personajes/" + _dbr.GetString(4) + "_agresor", typeof(AudioClip));
        sonidos[2] = (AudioClip)Resources.Load("Audios/Personajes/" + _dbr.GetString(4) + "_victima", typeof(AudioClip));
        equipoTemp[i].GetComponent<Personaje>().SetSonidos(sonidos);

        equipoTemp[i].GetComponent<SpriteRenderer>().sprite = (Sprite)Resources.Load("Sprites/Personajes/"+_dbr.GetString(4) + "_Cabeza", typeof(Sprite));
        equipoTemp[i].GetComponent<Personaje>().SetImagen((Sprite)Resources.Load("Sprites/Personajes/PanelDatos/" + _dbr.GetString(4) + "_Imagen", typeof(Sprite)));
        equipoTemp[i].GetComponent<Personaje>().SetNombrePrefab(_dbr.GetString(4) + "_Prefab");
        equipoTemp[i].GetComponent<Personaje>().SetNombre(_dbr.GetString(0));
        equipoTemp[i].GetComponent<Personaje>().SetMovimientos(int.Parse(_dbr.GetString(1)));
        equipoTemp[i].GetComponent<Personaje>().SetDanyo(int.Parse(_dbr.GetString(2)));
        equipoTemp[i].GetComponent<Personaje>().SetVida(int.Parse(_dbr.GetString(3)));
        equipoTemp[i].GetComponent<Personaje>().SetSuerte(int.Parse(_dbr.GetString(5)));
        equipoTemp[i].GetComponent<Personaje>().SetHabilidad(int.Parse(_dbr.GetString(6)));
    }

    private void RellenarEquipo(int eq) {

        string jefe, esbirros1, esbirros2;

        if (eq == 1) {
            jefe = A_jefe;
            esbirros1 = A_esbirros1;
            esbirros2 = A_esbirros2;
        } else { //Equipo 2
            jefe = B_jefe;
            esbirros1 = B_esbirros1;
            esbirros2 = B_esbirros2;
        }

        int i = 0;

        //Obtención del jefe

        _dbcm.CommandText = consulta + "\"" + jefe + "\"";
        _dbr = _dbcm.ExecuteReader();

        while (_dbr.Read()) {
            RellenarDatos(i);
            i++;
        }

        //Obtención de esbirros1

        _dbcm.CommandText = consulta + "\"" + esbirros1 + "\"";
        _dbr = _dbcm.ExecuteReader();

        while (_dbr.Read()) {
            RellenarDatos(i);
            i++;
        }

        //Obtención de esbirros2

        _dbcm.CommandText = consulta + "\"" + esbirros2 + "\"";
        _dbr = _dbcm.ExecuteReader();

        while (_dbr.Read()) {
            RellenarDatos(i);
            i++;
        }

    }

    private void ObtenerEquipo(int eq) {

        int a;
        GameObject equipo;

        if (eq == 1) {
            a = equipo1.transform.childCount;
            equipo = equipo1;
        } else { //Equipo 2
            a = equipo2.transform.childCount;
            equipo = equipo2;
        }

        equipoTemp = new GameObject[a];
        for (int i = 0; i < a; i++) {
            equipoTemp[i] = equipo.transform.GetChild(i).gameObject;
            //Debug.Log(equipoTemp[i]);
        }

        RellenarEquipo(eq);

    }

    // Método para obtener la lista de equipos para darsela al script "SeleccionarEquipo".
    public List<string[]> ObtenerListaEquipos()
    {
        _dbcm = _dbc.CreateCommand();
        _dbcm.CommandText = "SELECT * FROM equipos";
        _dbr = _dbcm.ExecuteReader();

        List<string[]> equipos = new List<string[]>();
        while (_dbr.Read())
        {
            string id = _dbr.GetString(0);
            string nombre = _dbr.GetString(1);
            string imagen = _dbr.GetString(2);
            equipos.Add(new string[3] { id, nombre, imagen });
        }

        return equipos;
    }

    // Método para obtener las imagenes de los personajes del equipo A de la base de datos.
    private void CargarImagenesEquipoA(int id)
    {
        _dbcm = _dbc.CreateCommand();
        _dbcm.CommandText = "SELECT nombreImagen FROM personajes WHERE id_equipo = " + id;
        _dbr = _dbcm.ExecuteReader();

        string[] resultado = new string[3];
        string comprobador = "";
        int i = 0;
        while (_dbr.Read())
        {
            string imagen = _dbr.GetString(0);
            if (!comprobador.Equals(imagen))
            {
                resultado[i] = imagen;
                i++;
            }
            comprobador = imagen;
        }

        foreach (string imagen in resultado)
        {
            string[] sliti = imagen.Split('_');
            switch (sliti[0])
            {
                case "Esbirro1":
                    A_esbirros1 = imagen;
                    break;
                case "Esbirro2":
                    A_esbirros2 = imagen;
                    break;
                case "Rey":
                    A_jefe = imagen;
                    break;
            }
        }
    }

    // Método para obtener las imagenes de los personajes del equipo B de la base de datos.
    private void CargarImagenesEquipoB(int id)
    {
        _dbcm = _dbc.CreateCommand();
        _dbcm.CommandText = "SELECT nombreImagen FROM personajes WHERE id_equipo = " + id;
        _dbr = _dbcm.ExecuteReader();

        string[] resultado = new string[3];
        string comprobador = "";
        int i = 0;
        while (_dbr.Read())
        {
            string imagen = _dbr.GetString(0);
            if (!comprobador.Equals(imagen))
            {
                resultado[i] = imagen;
                i++;
            }
            comprobador = imagen;
        }

        foreach (string imagen in resultado)
        {
            string[] sliti = imagen.Split('_');
            switch (sliti[0])
            {
                case "Esbirro1":
                    B_esbirros1 = imagen;
                    break;
                case "Esbirro2":
                    B_esbirros2 = imagen;
                    break;
                case "Rey":
                    B_jefe = imagen;
                    break;
            }
        }
    }

    // Método para buscar una habilidad en concreto y obtener su nombre.
    public string ObtenerNombreHabilidad(int idHabilidad)
    {
        _dbcm = _dbc.CreateCommand();
        _dbcm.CommandText = "SELECT nombre_habilidad FROM equipos WHERE id_habilidad = " + idHabilidad;
        _dbr = _dbcm.ExecuteReader();

        string nombreHabilidad = "";
        while (_dbr.Read())
        {
            nombreHabilidad = _dbr.GetString(0);
        }

        return nombreHabilidad;
    }
}
