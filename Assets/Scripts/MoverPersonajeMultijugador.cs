﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoverPersonajeMultijugador : MonoBehaviour {

    private static ControladorJuegoMultijugador controladorJuegoMultijugador;
    private static MarcarCasillas marcarCasillas;
    private static Interfaz interfaz;

    private ControlSelectorMultijugador controlSelectorMultijugador; //Instancia del selector.

    private Personaje pj;
    private int movimientoRestante;

    private int[,] movimientosPermitidos;

    private static GameObject panelFinMovimiento;
    public bool abiertoMenu = false;
    private bool moviendo = false;
    public bool atacando = false;
    private Vector2 coordOriginales;

    private EstadoJuego estadoJuego;

    private EstadoJuegoMultijugador estadoJuegoMultijugador;
    private TableroMultijugador tableroMultijugador;

    private void Awake()
    {
        estadoJuegoMultijugador = FindObjectOfType<EstadoJuegoMultijugador>();
        if (estadoJuegoMultijugador != null)
        {
            Destroy(GetComponent<MoverPersonaje>());
            tableroMultijugador = FindObjectOfType<TableroMultijugador>();


            estadoJuego = FindObjectOfType<EstadoJuego>();

            controlSelectorMultijugador = GameObject.FindWithTag("Selector").GetComponent<ControlSelectorMultijugador>();

            if (panelFinMovimiento == null) {
                panelFinMovimiento = GameObject.FindWithTag("PanelFinMovimiento");
                MenuAtkEsperar(false);
            }
        }

    }

    private void OnEnable()
    {

        Input.ResetInputAxes();

        if (controladorJuegoMultijugador == null)
        {
            controladorJuegoMultijugador = FindObjectOfType<ControladorJuegoMultijugador>();
            marcarCasillas = controladorJuegoMultijugador.GetComponent<MarcarCasillas>();
            interfaz = controladorJuegoMultijugador.GetComponent<Interfaz>();
        }

        pj = GetComponent<Personaje>();

        movimientoRestante = pj.GetMovimientos();
        marcarCasillas.MarcarMovsDisponibles(movimientoRestante, pj);

        moviendo = false;

        coordOriginales = transform.position;
    }

    private void Update()
    {

        if (moviendo == false)
        {
            //Al pulsar la tecla Enter se abrirá el menú para elegir qué acción realizar, solo si la casilla está disponible.
            if ((Input.GetKeyDown(KeyCode.Return) || (controladorJuegoMultijugador.turno == true && Input.GetButtonDown("J1-X/A")) || (controladorJuegoMultijugador.turno == false && Input.GetButtonDown("J2-X/A"))) && marcarCasillas.GetDisp())
            {
                MenuAtkEsperar(true);
                abiertoMenu = true;
                Mover();
                moviendo = true;
                controlSelectorMultijugador.estadoCursor = 2; //Bloqueamos el movimiento del cursor para evitar moverlo.
            }
        }

        if (controlSelectorMultijugador.enPausa == false && atacando == true && Input.GetKeyDown(KeyCode.Backspace) || (controladorJuegoMultijugador.turno == true && Input.GetButtonDown("J1-O/B")) || (controladorJuegoMultijugador.turno == false && Input.GetButtonDown("J2-O/B")))
        {
            atacando = false;
            marcarCasillas.LimpiarTablero();
            controlSelectorMultijugador.estadoCursor = 2;
            controlSelectorMultijugador.transform.position = transform.position;
            panelFinMovimiento.SetActive(true);
        }

    }

    public void MenuAtkEsperar(bool panel)
    {
        panelFinMovimiento.SetActive(panel);
    }

    public void Cancelar()
    {
        Retroceder();
        controlSelectorMultijugador.estadoCursor = 0;
    }

    public void Atacar()
    {
        MenuAtkEsperar(false);//Ocultamos el menú.
        abiertoMenu = false;
        marcarCasillas.AtkDisponibles(pj);//Mostramos las casillas disponibles para atacar, evitando las casillas de aliados.
        controlSelectorMultijugador.estadoCursor = 1;//Desbloqueamos el cursor para poder elegir la víctima.
        atacando = true;
    }

    public void EmpezarAtaque(Personaje personajeEnemigo)
    {
        if (marcarCasillas.GetDisp())
        {
            estadoJuego.SetCombatientes(pj, personajeEnemigo);
            controlSelectorMultijugador.estadoCursor = 2;
            marcarCasillas.LimpiarTablero();
            interfaz.OcultarPanelDatos();
            controladorJuegoMultijugador.GuardarPos(pj.transform.position);
            pj.FinTurno();
            SceneManager.LoadSceneAsync("Batalla_" + SceneManager.GetActiveScene().name, LoadSceneMode.Additive);
        }
    }

    public void Esperar()
    {
        pj.FinTurno(); //Para evitar volver a seleccionar el personaje cuando ya se haya movido en este turno.

        //Ocultamos el menú
        MenuAtkEsperar(false);
        abiertoMenu = false;
        controlSelectorMultijugador.estadoCursor = 0;
        controladorJuegoMultijugador.GuardarPos(pj.transform.position);

        enabled = false; //Desactivo el componente MoverPersonaje.
        tableroMultijugador.EnviarInstruccion("10:" + coordOriginales.x + ":" + coordOriginales.y + ":" + pj.transform.position.x + ":" + pj.transform.position.y);
    }

    public void Mover()
    {
        marcarCasillas.LimpiarTablero();

        //Muevo el personaje a la nueva posición.
        transform.position = controlSelectorMultijugador.transform.transform.position;

        //En el mapaPersonajes de controlSelector, voy a la posición donde está guardado el personaje y le asigno null,
        // ya que ahora irá a otra posición.
        controladorJuegoMultijugador.mapaPersonajes[(int)coordOriginales.x, (int)coordOriginales.y] = null;

        //En el mapaPersonajes en la posición nueva le asigno el GameObject del personaje que he movido.
        controladorJuegoMultijugador.mapaPersonajes[(int)controlSelectorMultijugador.transform.position.x, (int)controlSelectorMultijugador.transform.position.y] = gameObject;

    }

    public void Retroceder()
    {
        controladorJuegoMultijugador.mapaPersonajes[(int)transform.position.x, (int)transform.position.y] = null;
        transform.position = coordOriginales;
        controladorJuegoMultijugador.mapaPersonajes[(int)transform.position.x, (int)transform.position.y] = gameObject;
        controlSelectorMultijugador.gameObject.transform.position = transform.position;
        //Ocultamos el menú
        MenuAtkEsperar(false);
        abiertoMenu = false;
        enabled = false; //Desactivo el componente MoverPersonaje.
    }


    private void OnDisable()
    {
        //Justo antes de desactivar ejecutamos el método Continuar para indicar que estará listo para elegir otro personaje a mover.
        //C35757FF
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (GetComponent<Personaje>().GetTurno() == false) sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, 0.7f);//Reducir la opacidad del personaje si ya ha movido.
        controladorJuegoMultijugador.Continuar();
    }
}
