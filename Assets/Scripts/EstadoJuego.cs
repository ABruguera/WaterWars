﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EstadoJuego : MonoBehaviour {

    public static EstadoJuego estadoJuego;
    private Dictionary<int, string[]> equipos = new Dictionary<int, string[]>();
    private BaseDatos baseDatos;
    private bool baseDatosInicializada;

    // Atributos para la batalla.
    private Personaje agresor;
    private Personaje victima;

    public string nombreJugador1;
    public string nombreJugador2;

    // Se ejecuta antes que el método "Start".
    void Awake()
    {
        if (estadoJuego == null)
        {
            estadoJuego = this;
            baseDatosInicializada = false;
            DontDestroyOnLoad(gameObject);  // Hace que el objeto no se destruya al cargar otra escena.
        }
        else if (estadoJuego != this)
        {
            Destroy(gameObject);
        }
    }

    //
    private void Update()
    {
        if (SceneManager.GetActiveScene().Equals(SceneManager.GetSceneByName("TestMap")) && !baseDatosInicializada)
        {
            baseDatosInicializada = true;
            baseDatos = FindObjectOfType<BaseDatos>();
            baseDatos.InicializarEquipos();
        }
    }

    // Método para asignar los equipos que los jugadores han elegido.
    public void AsignarEquipos(string[] player1, string[] player2)
    {
        equipos.Add(1, player1);
        equipos.Add(2, player2);
    }

    public void AsignarNombresJugadores(string nombreJugador1, string nombreJugador2) {
        this.nombreJugador1 = nombreJugador1;
        this.nombreJugador2 = nombreJugador2;
    }

    // Método que retorna el identificador del equipo de Player 1.
    public int GetEquipo1()
    {
        return Int32.Parse(equipos[1][0]);
    }

    // Método que retorna el identificador del equipo de Player 2.
    public int GetEquipo2()
    {
        return Int32.Parse(equipos[2][0]);
    }

    // Método para asignar a los combatientes.
    public void SetCombatientes(Personaje agresor, Personaje victima)
    {
        this.agresor = agresor;
        this.victima = victima;
    }

    // Método que retorna al combatiente que ataca.
    public Personaje GetAgresor()
    {
        return agresor;
    }

    // Método que retorna al combatiente que va a ser atacado.
    public Personaje GetVictima()
    {
        return victima;
    }
}
