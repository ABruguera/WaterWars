﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Olas : MonoBehaviour {

    private static int anchoMapa;
    private static int altoMapa;

	void Start () {
        anchoMapa = GameObject.FindGameObjectWithTag("GameController").GetComponent<ControladorJuego>().ancho;
        altoMapa = GameObject.FindGameObjectWithTag("GameController").GetComponent<ControladorJuego>().alto;
        StartCoroutine(Movimiento());
	}
	
	IEnumerator Movimiento() {
        while (true) {
            while (transform.position.x < anchoMapa) {

                transform.localPosition += new Vector3(0.5f, 0f, 0f) * Time.deltaTime;
                yield return null;

            }

            yield return new WaitForSeconds(Random.Range(0.1f, 0.25f));

            NuevaPosicion();
        }
    }

    public void NuevaPosicion() {
        transform.localPosition = new Vector3(-1f, Random.Range(0f, altoMapa), -0.15f);
    }

    public void ResetPosicion() {
        transform.localPosition = new Vector3(transform.position.x - anchoMapa, Random.Range(0f, altoMapa), -0.15f);
    }

    private void OnTriggerEnter2D(Collider2D collision) {

        if (collision.CompareTag("Obstaculo")) { //Si la ola se encuentra de lleno con un objeto intocable, como una isla, la ola rompe y volverá a aparecer en otra posición.
             Invoke("NuevaPosicion", 3.4f);
        }
        
    }

}
