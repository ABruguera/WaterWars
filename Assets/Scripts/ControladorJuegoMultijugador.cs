﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControladorJuegoMultijugador : MonoBehaviour {

    //private static ControladorJuegoMultijugador controladorJuegoMultijugador; //Instancia para pasar a otros componentes.
    private Interfaz interfaz;
    public GameObject panelTerreno;

    public GameObject selector;
    public ControlSelectorMultijugador controlSelectorMultijugador;

    //Dimensiones del mapa.
    public int ancho;
    public int alto;

    //public GameObject[] personajesAliados; 
    public List<GameObject> personajesAliados; //Array con los personajes Aliados del tablero.
    public List<GameObject> personajesEnemigos; //Array con los personajes Enemigos del tablero.
    //public GameObject[] personajesEnemigos; 
    public GameObject[,] mapaPersonajes; //Array bidimensional simulando mapa donde se guardan los personajes.

    public bool turno; //True es turno de Aliados, False es turno de Enemigos.
    public string sTurno;

    //Aquí se guarda la posición del último movimiento, para así en su siguiente turno el cursor se moverá allí.
    private Vector3 ultimoMovimientoAliado;
    private Vector3 ultimoMovimientoEnemigo;

    private bool partidaSigue = true;

    private DesastresNaturales desastresNaturales;

    private bool primero = true;

    private TableroMultijugador tableroMultijugador;

    private void Awake()
    {

        //controladorJuegoMultijugador = this;
        if (FindObjectOfType<EstadoJuegoMultijugador>() != null)
        {
            GetComponent<ControladorJuego>().enabled = false;
            enabled = true;
        }
   
        interfaz = GetComponent<Interfaz>();

        mapaPersonajes = new GameObject[ancho, alto];

        //Recorremos el array de personajes y los asignamos a su posicion del mapa.
        foreach (GameObject pj in personajesAliados)
        {
            mapaPersonajes[(int)pj.transform.position.x, (int)pj.transform.position.y] = pj;
        }
        foreach (GameObject pj in personajesEnemigos)
        {
            mapaPersonajes[(int)pj.transform.position.x, (int)pj.transform.position.y] = pj;
        }

        desastresNaturales = GameObject.FindGameObjectWithTag("GeneradorDesastres").GetComponent<DesastresNaturales>();

        tableroMultijugador = GameObject.FindGameObjectWithTag("TableroMultijugador").GetComponent<TableroMultijugador>();
    }

   /* public static ControladorJuegoMultijugador GetInstancia()
    {
        return controladorJuegoMultijugador;
    }*/

    void Start()
    {
        panelTerreno.SetActive(false);
        PosicionesPrimerTurno();
        Obstaculos();
        CambioTurno();
        MostrarDatos();

    }

    public void Obstaculos()
    {
        GameObject[] obstaculos = GameObject.FindGameObjectsWithTag("Intocable");
        foreach (GameObject go in obstaculos)
        {
            mapaPersonajes[(int)go.transform.position.x, (int)go.transform.position.y] = go;
        }
    }

    public void PosicionesPrimerTurno()
    {
        ultimoMovimientoAliado = personajesAliados[0].transform.position;
        ultimoMovimientoEnemigo = personajesEnemigos[0].transform.position;
        selector.transform.position = ultimoMovimientoAliado;
    }


    public void AsignarUltimoMovimiento()
    {

        if (!turno)
        { //Contrario de lo que vale, si es True el último movimiento fue de los Aliados.
            selector.transform.position = ultimoMovimientoEnemigo; //Como el nuevo turno es los Enemigos, se le asigna al cursor la posicion del último movimiento hecho por ellos.
        }
        else
        {
            selector.transform.position = ultimoMovimientoAliado;
        }

    }

    public void ColorNormal()
    {
        List<GameObject> sad = GetPersonajesTurno();
        foreach (GameObject go in sad)
        {
            go.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    public void CambiarBoolTurno()
    {
        turno = !turno;
        if (turno == true)
        {
            sTurno = "Aliado";
        }
        else
        {
            sTurno = "Enemigo";
        }
    }

    private void ResetearEstados()
    {
        if (primero == false)
        {
            foreach (GameObject pj in GetPersonajesTurno())
            {
                Debug.Log("PUESSSSS   :    " + pj);
                pj.GetComponent<Personaje>().ResetEstado();
            }
        }
        else
        {
            primero = false;
        }
    }

    public void MostrarDatos()
    {
        GameObject personaje = mapaPersonajes[(int)selector.transform.position.x, (int)selector.transform.position.y];
        interfaz.MostrarDatosPersonaje(personaje);
    }

    public void CambioTurno()
    {

        ColorNormal();
        ResetearEstados();
        CambiarBoolTurno();

        List<GameObject> personajesNuevoTurno = GetPersonajesTurno();

        foreach (GameObject pj in personajesNuevoTurno)
        {
            pj.GetComponent<Personaje>().InicioTurno();
        }

        tableroMultijugador.turno = false;
        tableroMultijugador.EnviarInstruccion("20");

        interfaz.EntreTurno(turno);
    }

    public void Continuar()
    {

        if (partidaSigue == true)
        { //Partida sigue.
            bool quedanMovimientos = false;
            //Creo un nuevo array que apuntará a la lista de los personajes del turno actual, Aliados o Enemigos.
            List<GameObject> personajesTurnoActual = GetPersonajesTurno();

            foreach (GameObject pj in personajesTurnoActual)
            {
                if (pj.GetComponent<Personaje>().GetTurno() == true)
                {
                    //Si encuentra algún personaje al que le queden movimientos, ha de salir del bucle.
                    quedanMovimientos = true;
                    break;
                }
            }

            if (quedanMovimientos == false)
            {
                //Si no falta nadie por mover, inicia un nuevo turno.
                desastresNaturales.GenerarDesastre();
                CambioTurno();
                AsignarUltimoMovimiento();
                MostrarDatos();
            }

            controlSelectorMultijugador.moviendo = false;
        }
        else
        { //Fin de partida.
            FinalizarPartida();
        }

    }

    public List<GameObject> GetPersonajesTurno()
    {
        //Dependiendo del turno que sea, devuelvo la lista de Aliados o Enemigos para recorrerlo seguidamente.
        if (turno == true)
        {
            return personajesAliados;
        }
        else
        {
            return personajesEnemigos;
        }
    }

    public string GetTurno()
    {
        return this.sTurno;
    }

    public void PanelFinalizarTurno()
    {
        controlSelectorMultijugador.estadoCursor = 2;
        panelTerreno.SetActive(true);
    }

    public void CerrarPanelFinTurno()
    {
        panelTerreno.SetActive(false);
        controlSelectorMultijugador.estadoCursor = 0;
    }

    public void FinalizarTurno()
    {

        panelTerreno.SetActive(false);

        foreach (GameObject go in GetPersonajesTurno())
        {
            go.GetComponent<Personaje>().FinTurno();
        }

        CambioTurno();
        AsignarUltimoMovimiento();
        desastresNaturales.GenerarDesastre();
        MostrarDatos();
        controlSelectorMultijugador.estadoCursor = 0;
    }

    public void GuardarPos(Vector3 pos)
    {
        if (turno == true)
        {//Aliado
            ultimoMovimientoAliado = pos;
        }
        else
        {//Enemigo
            ultimoMovimientoEnemigo = pos;
        }
    }

    public void BorrarPersonaje(GameObject victima)
    {
        if (victima.CompareTag("Aliado"))
        {
            personajesAliados.Remove(victima);
        }
        else if (victima.CompareTag("Enemigo"))
        {
            personajesEnemigos.Remove(victima);
        }
    }

    public void ComprobarFinPartida()
    {

        List<GameObject> personajesEnemigos;

        if (turno == true)
        {
            personajesEnemigos = this.personajesEnemigos;
        }
        else
        {
            personajesEnemigos = this.personajesAliados;
        }

        if (personajesEnemigos.Count <= 0)
        {
            partidaSigue = false;
        }
    }

    public void FinalizarPartida()
    {
        controlSelectorMultijugador.gameObject.SetActive(false); //Desactivamos por completo el cursor.
        interfaz.FinPartida(turno);
    }


    public void Salir()
    {
        SceneManager.LoadScene(0); //Volvemos al menú principal.
    }

    public void Revancha()
    {
        Destroy(GameObject.FindGameObjectWithTag("EstadoJuego"));
        SceneManager.LoadScene(1);//Volvemos a la selección de equipo.
    }
}
