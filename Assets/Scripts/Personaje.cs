﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour{

    private Sprite imagen;
    private AudioSource audioSource;
    private AudioClip[] sonidos;
    private string nombre;

    private int movimientos;
    private int danyo;
    private int vida;
    private string nombrePrefab;

    private int suerte;  // Implementar (1 a 100).
    private int habilidad;  // Dependiendo del equipo tendrá una habilidad especial u otra.
    // La "suerte" servirá para la probabilidad de esquivar, para la frecuencia de crítico y para la habilidad especial.
    private int[] estadoAlterado;  // Indica el estado alterado:
                                  // 0 -> Nada; 1 -> Ceguera; 2 -> Hemorragia; 3 -> Envenenado; 4 -> Fulmina; 5 -> Confuso

    private bool faltaMover;
    private ControladorJuego controladorJuego;
    private Interfaz interfaz;

    //Métodos

    private void Start()
    {
        estadoAlterado = new int[2];
        controladorJuego = ControladorJuego.GetInstancia();
        interfaz = controladorJuego.GetComponent<Interfaz>();
        audioSource = GetComponent<AudioSource>();
    }

    public void InicioTurno() {
        GetComponent<SpriteRenderer>().color = Color.white;
        faltaMover = true;
    }

    public void FinTurno() {
        faltaMover = false;
    }

    //Getters & Setters

    public bool GetTurno() {
        return faltaMover;
    }

    public int GetMovimientos() {
        return movimientos;
    }

    public string GetNombre() {
        return this.nombre;
    }

    public Sprite GetImagen() {
        return this.imagen;
    }

    public int GetDanyo() {
        return this.danyo;
    }

    public int GetVida() {
        return this.vida;
    }

    public string GetNombrePrefab()
    {
        return nombrePrefab;
    }

    public int GetSuerte()
    {
        return suerte;
    }

    public int GetHabilidad()
    {
        return habilidad;
    }

    public int[] GetEstadoAlterado()
    {
        return estadoAlterado;
    }

    public void SetImagen(Sprite imagen) {
        this.imagen = imagen;
    }

    public void SetSonidos(AudioClip[] sonidos)
    {
        this.sonidos = sonidos;
    }

    public void SetNombre(string nombre) {
        this.nombre = nombre;
    }

    public void SetMovimientos(int movimientos) {
        this.movimientos = movimientos;
    }

    public void SetDanyo(int danyo) {
        this.danyo = danyo;
    }

    public void SetVida(int vida) {
        this.vida = vida;
    }

    public void SetNombrePrefab(string nombrePrefab)
    {
        this.nombrePrefab = nombrePrefab;
    }

    public void SetSuerte(int suerte)
    {
        this.suerte = suerte;
    }

    public void SetHabilidad(int habilidad)
    {
        this.habilidad = habilidad;
    }

    public void SetEstadoAlterado(int estadoAlterado)
    {
        if (estadoAlterado != -1) {

            if (this.estadoAlterado[0] == 0) this.estadoAlterado[0] = estadoAlterado;
            else if (this.estadoAlterado[0] != estadoAlterado) this.estadoAlterado[1] = estadoAlterado;//Para no tener doble veneno, por ejemplo.
        }
    }

    // Antes de resetear el estado alterado al final del turno del afectado, se ha de aplicar los posibles daños ocasionados.
    public void ResetEstado() {

        foreach (int estado in estadoAlterado) {
            switch (estado) {
                case 2:
                    this.vida -= 1;
                    ComprobarEstadoPersonaje();
                    break;
                case 3:
                    this.vida -= 2;
                    ComprobarEstadoPersonaje();
                    break;
            }
        }
        
        this.estadoAlterado[0] = 0;
        this.estadoAlterado[1] = 0;
    }

    // Método para aplicar el danyo que reciba en combate.
    public void AplicarDanyo(int danyo, int habilidad)
    {
        vida -= danyo;
        if (habilidad != -1 ) SetEstadoAlterado(habilidad);
    }

    public void Curar() {
        this.vida += 1;
    }


    // Métodos para comprobar si el personaje sigue o no con vida, para poder eliminarlo en caso de que ya no tenga.
    public bool ComprobarMuerto() {
        if (vida <= 0) {
            gameObject.SetActive(false);
            controladorJuego.mapaPersonajes[(int)transform.position.x, (int)transform.position.y] = null;
            controladorJuego.BorrarPersonaje(gameObject);
            if (controladorJuego.ComprobarFinPartida() == false) { //Si la partida todavía no ha terminado
                //Si el personaje muerto es el mismo al que está asignado en el cursor como último movimiento, 
                // se ha de cambiar para que el cursor en su turno no se mueva a una posición vacía, sino a uno vivo.
                if (CompareTag("Aliado") && transform.position == controladorJuego.ultimoMovimientoAliado) {
                    controladorJuego.ultimoMovimientoAliado = controladorJuego.personajesAliados[0].transform.position;
                } else if (CompareTag("Enemigo") && transform.position == controladorJuego.ultimoMovimientoEnemigo) {
                    controladorJuego.ultimoMovimientoAliado = controladorJuego.personajesEnemigos[0].transform.position;
                }
            }

            return true;
        }
        return false;
    }

    public void ComprobarEstadoPersonaje()
    {
        if (ComprobarMuerto() == false) {//Si no está muerto.
            //Actualizamos los datos que vemos por pantalla de él.
            interfaz.MostrarDatosPersonaje(gameObject);
        }
      
    }

    public void ReproducirSonidoTablero()
    {
        audioSource.clip = sonidos[0];
        audioSource.Play();
    }

    public void ReproducirSonidoAgresor()
    {
        audioSource.clip = sonidos[1];
        audioSource.Play();
    }

    public void ReproducirSonidoVictima()
    {
        audioSource.clip = sonidos[2];
        audioSource.Play();
    }
}
