﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IniciarConexion : MonoBehaviour {

    public static int PUERTO = 6669;  // Poner el puerto "6666" para conectarse con el Administrador de partidas.
    public static string HOST = "192.168.12.153";  // Especificar la dirección IP del servidor.
    private Socket sender;
    private EstadoJuegoMultijugador estadoJuegoMultijugador;

    // Atributos para el registro del usuario.
    public Text registrarUsuario;
    public InputField registrarContrasenya;
    public Text registrarNombre;
    public Text registrarApellidos;
    public Text registrarEmail;
    public Text registrarTelefono;
    public Text registrarResultado;

    // Atributos para el inicio de sesión del usuario.
    public Text identificarUsuario;
    public InputField identificarContrasenya;
    public Text identificarResultado;

    private void Awake()
    {
        estadoJuegoMultijugador = FindObjectOfType<EstadoJuegoMultijugador>();
    }
    
    // Use this for initialization
    void Start () {
        EstablecerConexion();
    }

    // Método para establecer la conexión con el servidor.
    private void EstablecerConexion()
    {
        try
        {
            // Establish the remote endpoint for the socket.
            // This example uses port 11000 on the local computer.
            /*IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11000);*/

            // Create a TCP/IP  socket.
            sender = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Connect the socket to the remote endpoint. Catch any errors.
            try
            {
                sender.Connect(HOST, PUERTO);
                print("Socket connected to " + sender.RemoteEndPoint.ToString());
            }
            catch (SocketException se)
            {
                print("SocketException : " + se.ToString());
            }
        }
        catch (Exception e)
        {
            print(e.ToString());
        }
    }

    // Método para calcular MD5.
    public string CalculateMD5Hash(string input)
    {
        MD5 md5 = System.Security.Cryptography.MD5.Create();
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
        byte[] hash = md5.ComputeHash(inputBytes);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }

        return sb.ToString();
    }

    // Método para iniciar sesión.
    public void IniciarSesion()
    {
        byte[] bytes = new byte[1024];  // Necesario para recibir los mensajes del servidor.
        byte[] msg = Encoding.ASCII.GetBytes("0\n");
        sender.Send(msg);
        int bytesRec = sender.Receive(bytes);
        // Espera el aviso del servidor para continuar con la petición.
        string mensaje = identificarUsuario.text + ":" + CalculateMD5Hash(identificarContrasenya.text);
        msg = Encoding.ASCII.GetBytes(mensaje + "\n");
        sender.Send(msg);

        bytesRec = sender.Receive(bytes);  // Recibe el mensaje del servidor en forma de bytes.
        byte opcion = Byte.Parse(Encoding.ASCII.GetString(bytes, 0, bytesRec));
        switch (opcion)
        {
            case 0:
                estadoJuegoMultijugador.SetUsuarioSalaMultijugador(identificarUsuario.text);
                //estadoJuegoMultijugador.SetEquipoPorDefecto(equipo);
                estadoJuegoMultijugador.SetServidorUsuarios(sender);
                SceneManager.LoadScene(5);
                break;
            case 1:
                identificarResultado.text = "Fail to start session";
                break;
            case 2:
                identificarResultado.text = "The user '" + identificarUsuario.text + "' doesn't exist";
                break;
        }
    }

    // Método para registrar a un nuevo usuario.
    public void CrearCuenta()
    {
        byte[] bytes = new byte[1024];  // Necesario para recibir los mensajes del servidor.
        byte[] msg = Encoding.ASCII.GetBytes("1\n");
        sender.Send(msg);  // Manda el mensaje al servidor.
        int bytesRec = sender.Receive(bytes);
        // Espera el aviso del servidor para continuar con la petición.
        string mensaje = registrarUsuario.text + ":" + CalculateMD5Hash(registrarContrasenya.text) + ":" + registrarNombre.text + ":" + registrarApellidos.text + ":" + registrarEmail.text + ":" + registrarTelefono.text;
        msg = Encoding.ASCII.GetBytes(mensaje + "\n");
        sender.Send(msg);
        bytesRec = sender.Receive(bytes);  // Recibe el mensaje del servidor en forma de bytes.
        registrarResultado.text = Encoding.ASCII.GetString(bytes, 0, bytesRec);  // Pasa de bytes a string el mensaje del servidor.
    }

    // Método para cerrar la conexión y volver al menú.
    public void VolverAlMenu()
    {
        byte[] msg = Encoding.ASCII.GetBytes("2\n");
        sender.Send(msg);
        sender.Close();
        SceneManager.LoadScene(0);
    }

    // 
    /*public void IniciarConexionConServidor()
    {
        try
        {
            // Establish the remote endpoint for the socket.
            // This example uses port 11000 on the local computer.
            IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11000);

            // Create a TCP/IP  socket.
            Socket sender = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Connect the socket to the remote endpoint. Catch any errors.
            try
            {
                sender.Connect(HOST, PUERTO);

                print("Socket connected to " + sender.RemoteEndPoint.ToString());

                // Encode the data string into a byte array.
                byte[] msg = Encoding.ASCII.GetBytes("This is a test<EOF>");

                // Send the data through the socket.
                int bytesSent = sender.Send(msg);

                // Receive the response from the remote device.
                int bytesRec = sender.Receive(bytes);
                Console.WriteLine("Echoed test = {0}",
                    Encoding.ASCII.GetString(bytes, 0, bytesRec));
                    
                // Release the socket.
                byte[] msg = Encoding.ASCII.GetBytes("Hola servidor, espero que puedas recibirme ;)");
                sender.Send(msg);
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();

            }
            catch (SocketException se)
            {
                print("SocketException : " + se.ToString());
            }

        }
        catch (Exception e)
        {
            print(e.ToString());
        }
    }*/
}
