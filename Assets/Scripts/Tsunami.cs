﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tsunami : MonoBehaviour {

    private ControlSelector controlSelector;
    private Interfaz interfaz;

    public byte tipo; // 1: Tsunami.
                      // 2: Petronami.

    private void Awake() {
        controlSelector = GameObject.FindGameObjectWithTag("Selector").GetComponent<ControlSelector>();
        interfaz = GameObject.FindGameObjectWithTag("GameController").GetComponent<Interfaz>();
    }

    void Start () {

        controlSelector.emergencia = true;
        controlSelector.estadoCursor = 2; //Bloqueamos el cursor durante la ola gigante.

        float velocidad;

        if (tipo == 1) {//Tsunami
            velocidad = 4f;
        } else {//Petronami
            velocidad = 3f;
        }
        
        GetComponent<Rigidbody2D>().velocity = Vector2.right * velocidad;


	}

    private void Update() {
        if (transform.position.x >= 23.3f) {
            controlSelector.estadoCursor = 0;
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {


        if (collision.CompareTag("Aliado") || collision.CompareTag("Enemigo")) {

            //Si es un tsunami, cura a los personajes que toque. Si es un petronami, les hace daño y envenena.
            if (tipo == 1) {
                collision.GetComponent<Personaje>().Curar();
            } else {
                collision.GetComponent<Personaje>().AplicarDanyo(2, 3);
                collision.GetComponent<Personaje>().ComprobarMuerto();
            }

            //Si el personaje al que golpea es el que tiene el cursor encima, actualiza los datos del panel:
            if (collision.gameObject.transform.position == controlSelector.gameObject.transform.position) {
                interfaz.MostrarDatosPersonaje(collision.gameObject);
            }

            //Si toca una ola, resetamos la posición de la ola, simulando que se la lleva.
        } else if (collision.CompareTag("Ola")) {
            collision.GetComponent<Olas>().ResetPosicion();
        }

    }

    private void OnDestroy() {
        controlSelector.emergencia = false;
    }

}
