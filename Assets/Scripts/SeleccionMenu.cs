﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SeleccionMenu : MonoBehaviour {

    public Button seleccionado;
    public GameObject sliderVolumen;
    private static ControladorJuego controladorJuego;

    private void Start() {
        if (controladorJuego == null) controladorJuego = ControladorJuego.GetInstancia();
    }
    //Seleccionamos la primera opción del menú.
    private void OnEnable() {
        if (sliderVolumen == null) { //Menú culquiera del juego.
            EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(seleccionado.gameObject);
            seleccionado.OnSelect(null);
        } else { //Menú de volumen del menú de opciones del menú principal 
            EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(sliderVolumen);
        }
        
    }

    private void Update() {
        if (controladorJuego != null) {
            if (controladorJuego.turno == true) {
                EventSystem.current.GetComponent<ControlMenus>().VerticalMando = "J1-FlechasArribaAbajo";
                EventSystem.current.GetComponent<ControlMenus>().SubmitMando = "J1-X/A";
            } else {
                EventSystem.current.GetComponent<ControlMenus>().VerticalMando = "J2-FlechasArribaAbajo";
                EventSystem.current.GetComponent<ControlMenus>().SubmitMando = "J2-X/A";
            }
        } 
    }

    //Para evitar problemas al cerrar el menú, por ejemplo seleccionar un personaje automáticamente cuando se cierra el menú con Cancelar.
    private void OnDisable() {
        Input.ResetInputAxes();
    }

}
