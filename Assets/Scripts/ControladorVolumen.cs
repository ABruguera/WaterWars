﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControladorVolumen : MonoBehaviour {

    private float vol;
    public Text txtVol;
    public Slider sliderVol;

    private void Start() {
        vol = PlayerPrefs.GetFloat("master_volume");
        txtVol.text = vol + "%";
        sliderVol.value = vol;
    }

    public void MostrarValor(float volumen) {
        txtVol.text = volumen + "%";
        this.vol = volumen;
    }
	
	public void Guardar() {
        PlayerPrefs.SetFloat("master_volume", vol); //Estableccemos una key con su valor para guardar el volumen introducido.
        PlayerPrefs.Save(); //Escribimos en el disco.
        AudioListener.volume = vol/100;
        SceneManager.LoadScene(0);
    }

}
