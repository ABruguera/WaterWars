using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoverPersonaje : MonoBehaviour {

    private static ControladorJuego controladorJuego;
    private static MarcarCasillas marcarCasillas;
    private static Interfaz interfaz;

    private ControlSelector controlSelector; //Instancia del selector.

    private Personaje pj;
    private int movimientoRestante;

    private int[,] movimientosPermitidos;

    private static GameObject panelFinMovimiento;
    public bool abiertoMenu = false;
    private bool moviendo = false;
    public bool atacando = false;
    private Vector2 coordOriginales;

    private EstadoJuego estadoJuego;

    private void Awake() {

        estadoJuego = FindObjectOfType<EstadoJuego>();

        if (FindObjectOfType<EstadoJuegoMultijugador>() == null) {

            Destroy(GetComponent<MoverPersonajeMultijugador>());

            controlSelector = GameObject.FindWithTag("Selector").GetComponent<ControlSelector>();

            if (panelFinMovimiento == null) {
                panelFinMovimiento = GameObject.FindWithTag("PanelFinMovimiento");
                MenuAtkEsperar(false);
            }
        }

    }

    private void OnEnable() {

        Input.ResetInputAxes();

        if (controladorJuego == null) {
            controladorJuego = ControladorJuego.GetInstancia();
            marcarCasillas = controladorJuego.GetComponent<MarcarCasillas>();
            interfaz = controladorJuego.GetComponent<Interfaz>();
        }

        pj = GetComponent<Personaje>();

        movimientoRestante = pj.GetMovimientos();
        marcarCasillas.MarcarMovsDisponibles(movimientoRestante, pj);

        moviendo = false;

        coordOriginales = transform.position;
    }

    private void Update() {

        if (moviendo == false) {
            //Al pulsar la tecla Enter se abrir� el men� para elegir qu� acci�n realizar, solo si la casilla est� disponible.
            if ((Input.GetKeyDown(KeyCode.Return) || (controladorJuego.turno == true && Input.GetButtonDown("J1-X/A")) || (controladorJuego.turno == false && Input.GetButtonDown("J2-X/A"))) && marcarCasillas.GetDisp()) {
                MenuAtkEsperar(true);
                abiertoMenu = true;
                Mover();
                moviendo = true;
                controlSelector.estadoCursor = 2; //Bloqueamos el movimiento del cursor para evitar moverlo.
            }
        }

        if (controlSelector.enPausa == false && atacando == true && Input.GetKeyDown(KeyCode.Backspace) || (controladorJuego.turno == true && Input.GetButtonDown("J1-O/B")) || (controladorJuego.turno == false && Input.GetButtonDown("J2-O/B"))) {
            atacando = false;
            marcarCasillas.LimpiarTablero();
            controlSelector.estadoCursor = 2;
            controlSelector.transform.position = transform.position;
            panelFinMovimiento.SetActive(true);
        }

    }

    public void MenuAtkEsperar(bool panel) {
        panelFinMovimiento.SetActive(panel);
    }

    public void Cancelar() {
        Retroceder();
        controlSelector.estadoCursor = 0;
    }

    public void Atacar() {
        MenuAtkEsperar(false);//Ocultamos el men�.
        abiertoMenu = false;
        marcarCasillas.AtkDisponibles(pj);//Mostramos las casillas disponibles para atacar, evitando las casillas de aliados.
        controlSelector.estadoCursor = 1;//Desbloqueamos el cursor para poder elegir la v�ctima.
        atacando = true;
    }

    public void EmpezarAtaque(Personaje personajeEnemigo) {
        if (marcarCasillas.GetDisp()) {
            estadoJuego.SetCombatientes(pj, personajeEnemigo);
            controlSelector.estadoCursor = 2;
            marcarCasillas.LimpiarTablero();
            interfaz.OcultarPanelDatos();
            controladorJuego.GuardarPos(pj.transform.position);
            pj.FinTurno();
            SceneManager.LoadSceneAsync("Batalla_" + SceneManager.GetActiveScene().name, LoadSceneMode.Additive);
        }
    }

    public void Esperar() {
        pj.FinTurno(); //Para evitar volver a seleccionar el personaje cuando ya se haya movido en este turno.
        
        //Ocultamos el men�
        MenuAtkEsperar(false);
        abiertoMenu = false;
        controlSelector.estadoCursor = 0;
        controladorJuego.GuardarPos(pj.transform.position);

        enabled = false; //Desactivo el componente MoverPersonaje.
    }

    public void Mover() {
        marcarCasillas.LimpiarTablero();

        //Muevo el personaje a la nueva posici�n.
        transform.position = controlSelector.transform.transform.position;

        //En el mapaPersonajes de controlSelector, voy a la posici�n donde est� guardado el personaje y le asigno null,
        // ya que ahora ir� a otra posici�n.
        controladorJuego.mapaPersonajes[(int)coordOriginales.x, (int)coordOriginales.y] = null;

        //En el mapaPersonajes en la posici�n nueva le asigno el GameObject del personaje que he movido.
        controladorJuego.mapaPersonajes[(int)controlSelector.transform.position.x, (int)controlSelector.transform.position.y] = gameObject;

    }

    public void Retroceder() {
        controladorJuego.mapaPersonajes[(int)transform.position.x, (int)transform.position.y] = null;
        transform.position = coordOriginales;
        controladorJuego.mapaPersonajes[(int)transform.position.x, (int)transform.position.y] = gameObject;
        controlSelector.gameObject.transform.position = transform.position;
        //Ocultamos el men�
        MenuAtkEsperar(false);
        abiertoMenu = false;
        enabled = false; //Desactivo el componente MoverPersonaje.
    }

    
    private void OnDisable() {
        //Justo antes de desactivar ejecutamos el m�todo Continuar para indicar que estar� listo para elegir otro personaje a mover.
        //C35757FF
        if (controladorJuego.partidaSigue == true){
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            if (GetComponent<Personaje>().GetTurno() == false) sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, 0.7f);//Reducir la opacidad del personaje si ya ha movido.
            controladorJuego.Continuar();
        }
        
    }

}
