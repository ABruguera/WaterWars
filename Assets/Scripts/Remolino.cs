﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Remolino : MonoBehaviour {

    private static ControlSelector controlSelector;
    private static Interfaz interfaz;
    public bool infinito = false;

    private void Awake() {
        controlSelector = GameObject.FindGameObjectWithTag("Selector").GetComponent<ControlSelector>();
        interfaz = GameObject.FindGameObjectWithTag("GameController").GetComponent<Interfaz>();
    }

    void Start () {

        if (infinito == false) {
            controlSelector.emergencia = true;
            controlSelector.estadoCursor = 2;
            int posX = Random.Range(2, 16);
            int posY = Random.Range(2, 9);
            transform.position = new Vector3(posX, posY, transform.position.z);
            StartCoroutine(QuitarRemolino());
        }

        StartCoroutine(Girar());

    }

    IEnumerator Girar() {
        while (true) {
            transform.Rotate(new Vector3(0f, 0f, 20f));
            yield return null;
        }
    }

    IEnumerator QuitarRemolino() {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
        controlSelector.estadoCursor = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Aliado") || (collision.CompareTag("Enemigo"))) {
            collision.GetComponent<Personaje>().SetEstadoAlterado(5);

            //Si el personaje al que golpea es el que tiene el cursor encima, actualiza los datos del panel:
            if (collision.gameObject.transform.position == controlSelector.gameObject.transform.position) {
                interfaz.MostrarDatosPersonaje(collision.gameObject);
            }
                
        }
    }

    private void OnDestroy() {
        controlSelector.emergencia = false;
    }
}
