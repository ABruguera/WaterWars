using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SeleccionarEquipo : MonoBehaviour {

    private BaseDatos baseDatos;
    private Dictionary<int, string[]> equipos = new Dictionary<int, string[]>();  // Se cargará desde la base de datos.
    private Sprite spritePlayer1;
    private Sprite spritePlayer2;
    public Image imagenPlayer1;
    public Image imagenPlayer2;
    public Text nombreEquiposPlayer1;
    public Text nombreEquiposPlayer2;
    private int totalPosiciones;
    private int posicionPlayer1;
    private int posicionPlayer2;
    public Animator animatorFlechaDerechaPlayer1;
    public Animator animatorFlechaIzquierdaPlayer1;
    public Animator animatorFlechaDerechaPlayer2;
    public Animator animatorFlechaIzquierdaPlayer2;
    private bool seleccionandoPlayer1;
    private bool seleccionandoPlayer2;
    private AudioSource audioSource;

    public InputField nombreJugador1;
    public InputField nombreJugador2;

    [Header("Mapas")]
    public string[] mapas;

    // Use this for initialization
    void Start () {
        InicializarImagenEquiposPlayer1();
        InicializarFlechasIzquierdas();
        baseDatos = FindObjectOfType<BaseDatos>();
        audioSource = GetComponent<AudioSource>();
        posicionPlayer1 = 1;
        posicionPlayer2 = 1;
        totalPosiciones = 0;
        InicializarEquipos();
        seleccionandoPlayer1 = false;
        seleccionandoPlayer2 = false;

        nombreJugador1.text = "Player 1";
        nombreJugador2.text = "Player 2";

    }

    // Método para rotar la imagen del Player 1 (para que mire hacia la derecha).
    private void InicializarImagenEquiposPlayer1()
    {
        Vector3 newScale = imagenPlayer1.gameObject.transform.localScale;
        newScale.x *= -1;
        imagenPlayer1.gameObject.transform.localScale = newScale;
    }

    // Método para rotar las flechas izquierdas del Player 1 y 2.
    private void InicializarFlechasIzquierdas()
    {
        // Player 1.
        Vector3 newScale = animatorFlechaIzquierdaPlayer1.gameObject.transform.localScale;
        newScale.x *= -1;
        animatorFlechaIzquierdaPlayer1.gameObject.transform.localScale = newScale;

        // Player 2.
        newScale = animatorFlechaIzquierdaPlayer2.gameObject.transform.localScale;
        newScale.x *= -1;
        animatorFlechaIzquierdaPlayer2.gameObject.transform.localScale = newScale;
    }

    // Método para inicializar todos los atributos de los equipos para que el usuario pueda seleccionar el que desee.
    private void InicializarEquipos()
    {
        // Cargamos los equipos desde la base de datos y los introducimos en nuestro diccionario.
        List<string[]> listaEquipos = baseDatos.ObtenerListaEquipos();
        foreach (string[] equipo in listaEquipos)
        {
            totalPosiciones++;
            equipos.Add(int.Parse(equipo[0]), equipo);
        }

        // Insertamos la imagen 1 por defecto.
        imagenPlayer1.sprite = (Sprite)Resources.Load("Sprites/SeleccionEquipo/" + equipos[posicionPlayer1][2], typeof(Sprite));
        imagenPlayer2.sprite = (Sprite)Resources.Load("Sprites/SeleccionEquipo/" + equipos[posicionPlayer1][2], typeof(Sprite));

        // Insertamos el nombre del equipo 1 por defecto.
        nombreEquiposPlayer1.text = equipos[posicionPlayer1][1];
        nombreEquiposPlayer2.text = equipos[posicionPlayer1][1];
    }

    // Update is called once per frame
    void Update () {
        // - Aquí llamaremos a los métodos para que los jugadores puedan cambiar de equipo
        // sin tener que pulsar con el ratón, es decir, lo harán con el teclado.
        ControlesPlayer1();
        ControlesPlayer2();
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("J1-X/A") || Input.GetButtonDown("J2-X/A")) BtnStart();
        else if (Input.GetKeyDown(KeyCode.Escape) || Input.GetButtonDown("J1-O/B") || Input.GetButtonDown("J2-O/B")) BtnBack();
    }

    // Método para avanzar una posición en el diccionario de equipos para el Player 1.
    public void FlechaDerechaPlayer1()
    {
        if (!seleccionandoPlayer1)
        {
            seleccionandoPlayer1 = true;
            StartCoroutine(EjecutarAnimacionFlechas(0));
            posicionPlayer1++;
            if (posicionPlayer1 > totalPosiciones)
            {
                posicionPlayer1 = 1;
            }
            imagenPlayer1.sprite = (Sprite)Resources.Load("Sprites/SeleccionEquipo/" + equipos[posicionPlayer1][2], typeof(Sprite));
            nombreEquiposPlayer1.text = equipos[posicionPlayer1][1];
        }
    }

    // Método para retroceder una posición en el diccionario de equipos para el Player 1.
    public void FlechaIzquierdaPlayer1()
    {
        if (!seleccionandoPlayer1)
        {
            seleccionandoPlayer1 = true;
            StartCoroutine(EjecutarAnimacionFlechas(1));
            posicionPlayer1--;
            if (posicionPlayer1 < 1)
            {
                posicionPlayer1 = totalPosiciones;
            }
            imagenPlayer1.sprite = (Sprite)Resources.Load("Sprites/SeleccionEquipo/" + equipos[posicionPlayer1][2], typeof(Sprite));
            nombreEquiposPlayer1.text = equipos[posicionPlayer1][1];
        }
    }

    // Método para avanzar una posición en el diccionario de equipos para el Player 2.
    public void FlechaDerechaPlayer2()
    {
        if (!seleccionandoPlayer2)
        {
            seleccionandoPlayer2 = true;
            StartCoroutine(EjecutarAnimacionFlechas(2));
            posicionPlayer2++;
            if (posicionPlayer2 > totalPosiciones)
            {
                posicionPlayer2 = 1;
            }
            imagenPlayer2.sprite = (Sprite)Resources.Load("Sprites/SeleccionEquipo/" + equipos[posicionPlayer2][2], typeof(Sprite));
            nombreEquiposPlayer2.text = equipos[posicionPlayer2][1];
        }
    }

    // Método para retroceder una posición en el diccionario de equipos para el Player 2.
    public void FlechaIzquierdaPlayer2()
    {
        if (!seleccionandoPlayer2)
        {
            seleccionandoPlayer2 = true;
            StartCoroutine(EjecutarAnimacionFlechas(3));
            posicionPlayer2--;
            if (posicionPlayer2 < 1)
            {
                posicionPlayer2 = totalPosiciones;
            }
            imagenPlayer2.sprite = (Sprite)Resources.Load("Sprites/SeleccionEquipo/" + equipos[posicionPlayer2][2], typeof(Sprite));
            nombreEquiposPlayer2.text = equipos[posicionPlayer2][1];
        }
    }

    // Método que se ejecutará cuando se pulse el botón "Start".
    public void BtnStart()
    {
        // Los equipos que se hayan elegido se guardarán en el diccionario de EstadoJuego.
        FindObjectOfType<EstadoJuego>().AsignarEquipos(equipos[posicionPlayer1], equipos[posicionPlayer2]);
        FindObjectOfType<EstadoJuego>().AsignarNombresJugadores(nombreJugador1.text, nombreJugador2.text);

        // Selección aleatoria de mapa.
        SceneManager.LoadScene(mapas[Random.Range(0, mapas.Length)]); 
    }

    // Método que se ejecutará cuando se pulse el botón "Back".
    public void BtnBack()
    {
        SceneManager.LoadScene("Menu_principal");
    }

    // Método para controlar los controles del Player1.
    private void ControlesPlayer1()
    {
        if (Input.GetKeyDown(KeyCode.D) || Input.GetAxis("J1-FlechasDerechaIzquierda") == 1) {
            FlechaDerechaPlayer1();
        } else if (Input.GetKeyDown(KeyCode.A) || Input.GetAxis("J1-FlechasDerechaIzquierda") == -1)
        {
            FlechaIzquierdaPlayer1();
        }
    }

    // Método para controlar los controles del Player2.
    private void ControlesPlayer2()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetAxis("J2-FlechasDerechaIzquierda") == 1)
        {
            FlechaDerechaPlayer2();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetAxis("J2-FlechasDerechaIzquierda") == -1)
        {
            FlechaIzquierdaPlayer2();
        }
    }

    // Método para animar la flecha la cual se utiliza para cambiar de equipo.
    IEnumerator EjecutarAnimacionFlechas(int flecha)
    {
        audioSource.Play();
        switch (flecha)
        {
            case 0:
                animatorFlechaDerechaPlayer1.SetBool("seleccionado", seleccionandoPlayer1);
                yield return new WaitForSeconds(0.5f);
                seleccionandoPlayer1 = false;
                animatorFlechaDerechaPlayer1.SetBool("seleccionado", seleccionandoPlayer1);
                break;
            case 1:
                animatorFlechaIzquierdaPlayer1.SetBool("seleccionado", seleccionandoPlayer1);
                yield return new WaitForSeconds(0.5f);
                seleccionandoPlayer1 = false;
                animatorFlechaIzquierdaPlayer1.SetBool("seleccionado", seleccionandoPlayer1);
                break;
            case 2:
                animatorFlechaDerechaPlayer2.SetBool("seleccionado", seleccionandoPlayer2);
                yield return new WaitForSeconds(0.5f);
                seleccionandoPlayer2 = false;
                animatorFlechaDerechaPlayer2.SetBool("seleccionado", seleccionandoPlayer2);
                break;
            case 3:
                animatorFlechaIzquierdaPlayer2.SetBool("seleccionado", seleccionandoPlayer2);
                yield return new WaitForSeconds(0.5f);
                seleccionandoPlayer2 = false;
                animatorFlechaIzquierdaPlayer2.SetBool("seleccionado", seleccionandoPlayer2);
                break;
        }
    }
}
