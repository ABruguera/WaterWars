using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSelector : MonoBehaviour {

    private static ControladorJuego controladorJuego;
    public Interfaz interfaz;


    private GameObject personaje; //Personaje que seleccionamos. Lo usamos para poder activar su componente MoverPersonaje.

    public bool moviendo = false; //Para evitar seleccionar otro personaje cuando estamos moviendo ya a uno.
    public byte estadoCursor = 0; //Para evitar mover el cursor en ciertas situaciones.
                                  // 0 = desbloqueado; mayoría del tiempo.
                                  // 1 = para obtener un personaje rival.
                                  // 2 = bloqueado.

    private Personaje personajeEnemigo;

    public GameObject pausa;
    private byte cursorPausa;
    public bool enPausa = false;
    private bool abiertoMenuAcciones = false;
    private bool abiertoPanelTerreno = false;

    private bool J1DPADEnUsoX = false;
    private bool J1DPADEnUsoY = false;
    private bool J2DPADEnUsoX = false;
    private bool J2DPADEnUsoY = false;

    public bool emergencia;

    private void Start() {
        if (controladorJuego == null) {
            controladorJuego = ControladorJuego.GetInstancia();
        }
    }

    public void MovimientoDelCursor() {

        //Dependiendo del turno, el cursor se moverá con WASD (aliados/J1) o con flechas (enemigos/J2).
        //turno == true : aliados. turno == false : enemigos.

        if (controladorJuego.turno == true) { //Aliados / J1

            if (Input.GetKeyDown(KeyCode.D) || (Input.GetAxis("J1-FlechasDerechaIzquierda") == 1 && J1DPADEnUsoX == false)) {
                MoverSelector(Vector3.right);
                J1DPADEnUsoX = true; //Así solo hace un único movimiento con el dpad del mando.
            }

            if (Input.GetKeyDown(KeyCode.A) || (Input.GetAxis("J1-FlechasDerechaIzquierda") == -1 && J1DPADEnUsoX == false)) {
                MoverSelector(Vector3.left);
                J1DPADEnUsoX = true;
            }

            if (Input.GetAxis("J1-FlechasDerechaIzquierda") == 0) { //Limpiamos
                J1DPADEnUsoX = false;
            }

            if (Input.GetKeyDown(KeyCode.W) || (Input.GetAxis("J1-FlechasArribaAbajo") == 1 && J1DPADEnUsoY == false)) {
                MoverSelector(Vector3.up);
                J1DPADEnUsoY = true;
            }

            if (Input.GetKeyDown(KeyCode.S) || (Input.GetAxis("J1-FlechasArribaAbajo") == -1 && J1DPADEnUsoY == false)) {
                MoverSelector(Vector3.down);
                J1DPADEnUsoY = true;
            }

            if (Input.GetAxis("J1-FlechasArribaAbajo") == 0) { //Limpiamos
                J1DPADEnUsoY = false;
            }


        } else { //Enemigos / J2

            if (Input.GetKeyDown(KeyCode.RightArrow) || (Input.GetAxis("J2-FlechasDerechaIzquierda") == 1 && J2DPADEnUsoX == false)) {
                MoverSelector(Vector3.right);
                J2DPADEnUsoX = true;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow) || (Input.GetAxis("J2-FlechasDerechaIzquierda") == -1 && J2DPADEnUsoX == false)) {
                MoverSelector(Vector3.left);
                J2DPADEnUsoX = true;
            }

            if (Input.GetAxis("J2-FlechasDerechaIzquierda") == 0) { //Limpiamos
                J2DPADEnUsoX = false;
            }

            if (Input.GetKeyDown(KeyCode.UpArrow) || (Input.GetAxis("J2-FlechasArribaAbajo") == 1 && J2DPADEnUsoY == false)) {
                MoverSelector(Vector3.up);
                J2DPADEnUsoY = true;
            }

            if (Input.GetKeyDown(KeyCode.DownArrow) || (Input.GetAxis("J2-FlechasArribaAbajo") == -1 && J2DPADEnUsoY == false)) {
                MoverSelector(Vector3.down);
                J2DPADEnUsoY = true;
            }

            if (Input.GetAxis("J2-FlechasArribaAbajo") == 0) { //Limpiamos
                J2DPADEnUsoY = false;
            }

        }
    }

    void Update() {

        if (estadoCursor == 0 || estadoCursor == 1) {

            MovimientoDelCursor();

            //Si no estamos moviendo un personaje y si hemos pulsado la tecla Enter:
            if (moviendo == false && (Input.GetKeyDown(KeyCode.Return) || (controladorJuego.turno == true && Input.GetButtonDown("J1-X/A")) || (controladorJuego.turno == false && Input.GetButtonDown("J2-X/A")))) {
                ControlarPersonajePropio();
            }

            if (estadoCursor == 1 && (Input.GetKeyDown(KeyCode.Return) || (controladorJuego.turno == true && Input.GetButtonDown("J1-X/A")) || (controladorJuego.turno == false && Input.GetButtonDown("J2-X/A")))) {
                ObtenerVictima();
            }

        }

        //Con mando, solo puedes poner pausa si es tu turno.
        if (enPausa == false && emergencia == false && (Input.GetKeyDown(KeyCode.Escape) || (controladorJuego.turno == true && Input.GetButtonDown("J1-Start")) || (controladorJuego.turno == false && Input.GetButtonDown("J2-Start")))) {
            //Si el menú de movimiento (atacar, esperar, cancelar) está abierto, se ha de ocultar. Lo mismo en que menú de termianr turno del terreno.
            if (personaje != null) abiertoMenuAcciones = personaje.GetComponent<MoverPersonaje>().abiertoMenu;
            if (abiertoMenuAcciones == true) personaje.GetComponent<MoverPersonaje>().MenuAtkEsperar(false);
            abiertoPanelTerreno = controladorJuego.panelTerreno.activeInHierarchy;
            if (abiertoPanelTerreno == true) controladorJuego.panelTerreno.SetActive(false);

            enPausa = true;
            cursorPausa = estadoCursor;
            estadoCursor = 2;
            pausa.SetActive(true);
        }

    }

    public void ContinuarPausa() {
        //Volver a mostrar los menús que ocultamos si estaban activos.
        if (abiertoMenuAcciones == true) personaje.GetComponent<MoverPersonaje>().MenuAtkEsperar(true);
        if (abiertoPanelTerreno == true) controladorJuego.panelTerreno.SetActive(true);

        estadoCursor = cursorPausa;
        pausa.SetActive(false);
        enPausa = false;
    }

    public void AbandonarPausa() {
        pausa.SetActive(false);
        interfaz.AbandonarPartidaPausa(controladorJuego.turno);
        controladorJuego.CambiarBoolTurno();
        controladorJuego.FinalizarPartida();
    }

    public void ControlarPersonajePropio() {
        //Obtenemos el personaje del mapa situado en la misma posición del cursor.
        personaje = controladorJuego.mapaPersonajes[(int)transform.position.x, (int)transform.position.y];
        if (personaje != null && personaje.CompareTag(controladorJuego.GetTurno())) { //Si hay personaje del jugador actual.

            if (personaje.GetComponent<Personaje>().GetTurno() == true) {
                moviendo = true;
                personaje.GetComponent<MoverPersonaje>().enabled = true;//Activamos el componente MoverPersonaje del seleccionado.
                personaje.GetComponent<Personaje>().ReproducirSonidoTablero();
            }

        } else if (personaje == null) { //Si se pulsa en terreno vacío.
            controladorJuego.PanelFinalizarTurno();
        }
    }

    public void ObtenerVictima() {
        GameObject pj = controladorJuego.mapaPersonajes[(int)transform.position.x, (int)transform.position.y];
        if (pj != null) { //Si hay personaje enemigo. // && !pj.CompareTag(controladorJuego.GetTurno()) && marcarCasillas.GetDisp()

           personajeEnemigo = pj.GetComponent<Personaje>();
           personaje.GetComponent<MoverPersonaje>().EmpezarAtaque(personajeEnemigo); 

        }
    }

    public void Esperar() {
        personaje.GetComponent<MoverPersonaje>().Esperar();
    }

    public void Atacar() {
        personaje.GetComponent<MoverPersonaje>().Atacar();
    }

    public void Cancelar() {
        personaje.GetComponent<MoverPersonaje>().Cancelar();
    }

    public void MoverSelector(Vector3 direccion) {
        transform.position += direccion;
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, 0f, controladorJuego.ancho - 1), Mathf.Clamp(transform.position.y, 0f, controladorJuego.alto - 1));

        GameObject personaje = controladorJuego.mapaPersonajes[(int)transform.position.x, (int)transform.position.y];
        interfaz.MostrarDatosPersonaje(personaje);
    }

}
