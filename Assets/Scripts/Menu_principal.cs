﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu_principal : MonoBehaviour {

    private void Start()
    {
        IniciarComprobaciones();
        AudioListener.volume = PlayerPrefs.GetFloat("master_volume")/100;
    }

    // Método que se ejecutará cuando se pulse el botón "Local game".
    public void BtnLocalGame()
    {
        SceneManager.LoadScene("Seleccionar_equipos_local");
    }

    // Método que se ejecutará cuando se pulse el botón "Network game".
    public void BtnNetworkGame()
    {
        SceneManager.LoadScene(4);
    }

    public void BtnOpciones() {
        SceneManager.LoadScene(13);
    }

    // Método que se ejecutará cuando se pulse el botón "Exit game".
    public void BtnExitGame()
    {
        Application.Quit();  // Cierra la aplicación.
    }

    // Método para iniciar las comprobaciones necesarias para iniciar el menú principal.
    private void IniciarComprobaciones()
    {
        // Borra el objeto "EstadoJuego" en caso de que exista.
        GameObject estadoJuego = FindObjectOfType<EstadoJuego>().gameObject;
        if (estadoJuego != null)
        {
            GameObject.Destroy(estadoJuego);
        }

        // Bora el objeto "EstadoJuegoMultijugador" en caso de que exista.
        GameObject estadoJuegoMultijugador = FindObjectOfType<EstadoJuegoMultijugador>().gameObject;
        if (estadoJuegoMultijugador != null)
        {
            GameObject.Destroy(estadoJuegoMultijugador);
        }
    }
}
