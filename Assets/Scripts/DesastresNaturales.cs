﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesastresNaturales : MonoBehaviour {

    public int turno = 0;
    public GameObject tsunami;
    public GameObject petronami;
    public GameObject remolino;


    public void GenerarDesastre() {
        
        if (Random.Range(1,101) <= 40) { // 40% de posibilidades de que tras un turno ocurra un desastre.

            switch (Random.Range(1, 4)){
                case 1://Tsunami
                    //Cura 1 punto de vida a todos los personajes del campo de batalla.
                    Instantiate(tsunami).GetComponent<Tsunami>().tipo = 1;
                    break;
                case 2://Petronami
                    //Hace 2 puntos de daño y envenena.
                    Instantiate(petronami).GetComponent<Tsunami>().tipo = 2;
                    break;
                case 3: //Remolinos
                    //Confunde a quien toca.
                    Instantiate(remolino);
                    Instantiate(remolino);
                    break;
            }

        }
    }

}
