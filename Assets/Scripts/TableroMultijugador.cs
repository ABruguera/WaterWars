﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class TableroMultijugador : MonoBehaviour {

    private EstadoJuegoMultijugador estadoJuegoMultijugador;
    private static ControladorJuegoMultijugador controladorJuegoMultijugador;
    private ControlSelectorMultijugador selectorMulti;
    private Socket socketPartida;

    public Button btnMover;
    public bool turno;

    private void Awake() {
        estadoJuegoMultijugador = FindObjectOfType<EstadoJuegoMultijugador>();
        if (estadoJuegoMultijugador == null) {
            Destroy(gameObject);
        } else {
            estadoJuegoMultijugador.CargarDatosJugador();
        }

    }

    private void OnEnable()
    {
         controladorJuegoMultijugador = FindObjectOfType<ControladorJuegoMultijugador>();
         estadoJuegoMultijugador.baseDatos = FindObjectOfType<BaseDatos>();
         selectorMulti = FindObjectOfType<ControlSelectorMultijugador>();
    }

    // Use this for initialization
    void Start()
    {
        estadoJuegoMultijugador.CargarJugadores();
        // Desactiva el selector normal y activa el selector multijugador.
        selectorMulti.GetComponent<ControlSelector>().enabled = false;
        selectorMulti.enabled = true;

        // Cambiamos el onlick al ser multijugador, tiene que llamar al selector del multi.
        btnMover.onClick.RemoveAllListeners();
        btnMover.onClick.AddListener(() =>  selectorMulti.Esperar());

        socketPartida = estadoJuegoMultijugador.GetSocketPartida();

        turno = estadoJuegoMultijugador.GetEsJugador1();

        //StartCoroutine(Esperanza());
    }

    public void EnviarInstruccion(string instruccion) {
        byte[] msg = Encoding.ASCII.GetBytes(instruccion+"\n");
        socketPartida.Send(msg);
    }


    IEnumerator Esperanza() {

        while (!turno) {

            if (!turno) {
                yield return null;
                byte[] bytes = new byte[1024];
                int bytesRec = socketPartida.Receive(bytes);
                int[] op = Array.ConvertAll(Encoding.ASCII.GetString(bytes, 0, bytesRec).Split(':'), int.Parse);

                switch (op[0]) {

                    case 20:
                        turno = true;
                        break;
                    case 10:
                        //Accion:Vector2Personaje:Vector2:NuevaPos
                        Debug.Log("Ha movido");
                        Mover(new Vector2(op[1], op[2]), new Vector2(op[3], op[4]));
                        break;
                    case 11:
                        //atacar
                        break;
                    case 12:
                        //esperar
                        break;
                }
                Debug.Log("KIKIKIKIKIKI");
                yield return new WaitForSeconds(1f);
            }

            yield return null;

        }
        
    }

    private void Mover(Vector2 personaje, Vector2 destino) {
        Personaje pj = controladorJuegoMultijugador.mapaPersonajes[(int)personaje.x, (int)personaje.y].GetComponent<Personaje>();
        controladorJuegoMultijugador.mapaPersonajes[(int)personaje.x, (int)personaje.y] = null;
        controladorJuegoMultijugador.mapaPersonajes[(int)destino.x, (int)destino.y] = gameObject;
        pj.gameObject.transform.position = destino;
        //pj.FinTurno();
        pj.enabled = false;
    }

}
