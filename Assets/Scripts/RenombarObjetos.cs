﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script para modificar gran cantidad de objetos de una tirada y se queda guardado en el editor.
[ExecuteInEditMode]
public class RenombarObjetos : MonoBehaviour {

    public GameObject Original;

    void Start() {
        Transform[] allChildren = Original.GetComponentsInChildren<Transform>();

        foreach (Transform child in allChildren) {
            child.name = "Agua";
        }
        print(Original.transform.childCount);
    }
}
