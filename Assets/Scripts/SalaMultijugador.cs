﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SalaMultijugador : MonoBehaviour {

    private EstadoJuegoMultijugador estadoJuegoMultijugador;
    public GameObject panelPrincipal;
    public GameObject panelSeleccionEquipo;
    public Text usuario;

    private Dictionary<int, string[]> equipos = new Dictionary<int, string[]>();
    private BaseDatos baseDatos;
    private int totalPosiciones;
    public Text nombreEquiposPlayer1;
    public Image imagenPlayer1;
    private int posicionPlayer1;
    private bool seleccionandoPlayer1;
    public Transform flechaDerecha;
    public Transform flechaIzquierda;
    private AudioSource audioSource;

    public Image imagenRey;
    public Image imagenEsbirro1;
    public Image imagenEsbirro2;

    private void Awake()
    {
        baseDatos = FindObjectOfType<BaseDatos>();
        estadoJuegoMultijugador = FindObjectOfType<EstadoJuegoMultijugador>();
        audioSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start () {
        estadoJuegoMultijugador.CargarDatosJugador();
        totalPosiciones = 0;
        posicionPlayer1 = estadoJuegoMultijugador.GetEquipoPorDefecto();
        InicializarEquipos();
        panelSeleccionEquipo.SetActive(false);
        usuario.text = estadoJuegoMultijugador.GetUsuarioSalaMultijugador();
    }

    // Método para inicializar todos los atributos de los equipos para que el usuario pueda seleccionar el que desee.
    private void InicializarEquipos()
    {
        // Cargamos los equipos desde la base de datos y los introducimos en nuestro diccionario.
        List<string[]> listaEquipos = baseDatos.ObtenerListaEquipos();
        foreach (string[] equipo in listaEquipos)
        {
            totalPosiciones++;
            int numEquipo = int.Parse(equipo[0]);
            equipos.Add(numEquipo, equipo);
        }

        // Insertamos la imagen 1 por defecto.
        imagenPlayer1.sprite = (Sprite)Resources.Load("Sprites/SeleccionEquipo/" + equipos[posicionPlayer1][2], typeof(Sprite));

        // Insertamos el nombre del equipo 1 por defecto.
        nombreEquiposPlayer1.text = equipos[posicionPlayer1][1];

        // Mostramos los integrantes del equipo en las imagenes.
        imagenRey.sprite = (Sprite)Resources.Load("Sprites/Personajes/CuerpoEntero/" + equipos[posicionPlayer1][2] + "_rey", typeof(Sprite));
        imagenEsbirro1.sprite = (Sprite)Resources.Load("Sprites/Personajes/CuerpoEntero/" + equipos[posicionPlayer1][2] + "_esbirro1", typeof(Sprite));
        imagenEsbirro2.sprite = (Sprite)Resources.Load("Sprites/Personajes/CuerpoEntero/" + equipos[posicionPlayer1][2] + "_esbirro2", typeof(Sprite));
    }

    // Update is called once per frame
    void Update () {
		
	}

    // Método para iniciar la partida online.
    public void BuscarOponente()
    {
        estadoJuegoMultijugador.IniciarPartida();
        estadoJuegoMultijugador.GetUsuarios();
        estadoJuegoMultijugador.Notificar();
        SceneManager.LoadScene("TestMap");
        
          // Abre la escena del tablero multijugador.
    }

    // Método para cambiar el equipo del jugador.
    public void CambiarEquipo()
    {
        panelPrincipal.SetActive(false);
        panelSeleccionEquipo.SetActive(true);
    }

    // Método para avanzar una posición en el diccionario de equipos para el Player 1.
    public void FlechaDerechaPlayer1()
    {
        if (!seleccionandoPlayer1)
        {
            seleccionandoPlayer1 = true;
            StartCoroutine(EjecutarAnimacionFlechas(0));
            posicionPlayer1++;
            if (posicionPlayer1 > totalPosiciones)
            {
                posicionPlayer1 = 1;
            }
            imagenPlayer1.sprite = (Sprite)Resources.Load("Sprites/SeleccionEquipo/" + equipos[posicionPlayer1][2], typeof(Sprite));
            imagenRey.sprite = (Sprite)Resources.Load("Sprites/Personajes/CuerpoEntero/" + equipos[posicionPlayer1][2] + "_rey", typeof(Sprite));
            imagenEsbirro1.sprite = (Sprite)Resources.Load("Sprites/Personajes/CuerpoEntero/" + equipos[posicionPlayer1][2] + "_esbirro1", typeof(Sprite));
            imagenEsbirro2.sprite = (Sprite)Resources.Load("Sprites/Personajes/CuerpoEntero/" + equipos[posicionPlayer1][2] + "_esbirro2", typeof(Sprite));
            nombreEquiposPlayer1.text = equipos[posicionPlayer1][1];
        }
    }

    // Método para retroceder una posición en el diccionario de equipos para el Player 1.
    public void FlechaIzquierdaPlayer1()
    {
        if (!seleccionandoPlayer1)
        {
            seleccionandoPlayer1 = true;
            StartCoroutine(EjecutarAnimacionFlechas(1));
            posicionPlayer1--;
            if (posicionPlayer1 < 1)
            {
                posicionPlayer1 = totalPosiciones;
            }
            imagenPlayer1.sprite = (Sprite)Resources.Load("Sprites/SeleccionEquipo/" + equipos[posicionPlayer1][2], typeof(Sprite));
            imagenRey.sprite = (Sprite)Resources.Load("Sprites/Personajes/CuerpoEntero/" + equipos[posicionPlayer1][2] + "_rey", typeof(Sprite));
            imagenEsbirro1.sprite = (Sprite)Resources.Load("Sprites/Personajes/CuerpoEntero/" + equipos[posicionPlayer1][2] + "_esbirro1", typeof(Sprite));
            imagenEsbirro2.sprite = (Sprite)Resources.Load("Sprites/Personajes/CuerpoEntero/" + equipos[posicionPlayer1][2] + "_esbirro2", typeof(Sprite));
            nombreEquiposPlayer1.text = equipos[posicionPlayer1][1];
        }
    }

    // Método para animar la flecha la cual se utiliza para cambiar de equipo.
    IEnumerator EjecutarAnimacionFlechas(int flecha)
    {
        audioSource.Play();
        switch (flecha)
        {
            case 0:
                float inicial = flechaDerecha.position.x;
                float i = inicial;
                float j = i + 5;
                while (i < j) {
                    i++;
                    flechaDerecha.position = new Vector2(i, flechaDerecha.position.y);
                    yield return null;
                }
                while (inicial < i)
                {
                    i--;
                    flechaDerecha.position = new Vector2(i, flechaDerecha.position.y);
                    yield return null;
                }
                break;
            case 1:
                float inicial2 = flechaIzquierda.position.x;
                float i2 = inicial2;
                float j2 = i2 - 5;
                while (i2 > j2)
                {
                    i2--;
                    flechaIzquierda.position = new Vector2(i2, flechaIzquierda.position.y);
                    yield return null;
                }
                while (inicial2 > i2)
                {
                    i2++;
                    flechaIzquierda.position = new Vector2(i2, flechaIzquierda.position.y);
                    yield return null;
                }
                break;
        }
        seleccionandoPlayer1 = false;
    }

    // Método para confirmar el cambio de equipo y volver al panel principal.
    public void SeleccionarEquipo()
    {
        estadoJuegoMultijugador.CambiarEquipo(equipos[posicionPlayer1][0]);  // Por el momento solamente cambiará al equipo 2.
        panelSeleccionEquipo.SetActive(false);
        panelPrincipal.SetActive(true);
    }

    // Método para cerrar sesión y volver al menú principal.
    public void CerrarSesion()
    {
        estadoJuegoMultijugador.CerrarConexion();
        SceneManager.LoadScene(0);
    }
}
