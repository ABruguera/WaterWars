using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interfaz : MonoBehaviour {


    public Text txtTurno;
    public GameObject panelFinPartida;
    public Text finPartida;
    public Text abandonado;

    [Header("Panel Datos Personaje")]
    public GameObject panelDatosPersonaje;
    public Image imagen;
    public Text nombre;
    public Text hp;
    public Text atk;
    public Image estado;
    public Image estado2;

    private string nombreJugador1;
    private string nombreJugador2;

    private EstadoJuegoMultijugador estadoJuegoMultijugador;

    private void Awake() {
        estadoJuegoMultijugador = FindObjectOfType<EstadoJuegoMultijugador>();
        if (estadoJuegoMultijugador == null)
        {
            nombreJugador1 = GameObject.FindGameObjectWithTag("EstadoJuego").GetComponent<EstadoJuego>().nombreJugador1;
            nombreJugador2 = GameObject.FindGameObjectWithTag("EstadoJuego").GetComponent<EstadoJuego>().nombreJugador2;
        } else
        {
            nombreJugador1 = estadoJuegoMultijugador.nombreJugador1;
            nombreJugador2 = estadoJuegoMultijugador.nombreJugador2;
        }
    }

   public void MostrarDatosPersonaje(GameObject pj) {
        
        if (pj != null && !pj.CompareTag("Intocable")) {
            if (pj.transform.position.x < 8) {
                panelDatosPersonaje.transform.localPosition = new Vector2(409.42f, 303.4f);//Posición relativa al parent.
                imagen.transform.localScale = new Vector2(1f, 1f);
            } else {
                panelDatosPersonaje.transform.localPosition = new Vector2(-568.18f, 303.4f);
                imagen.transform.localScale = new Vector2(-1f, 1f);
            }
            panelDatosPersonaje.SetActive(true);
            imagen.sprite = pj.GetComponent<Personaje>().GetImagen();
            nombre.text = pj.GetComponent<Personaje>().GetNombre();
            hp.text = pj.GetComponent<Personaje>().GetVida().ToString();
            atk.text = pj.GetComponent<Personaje>().GetDanyo().ToString();

            int[] numEstado = pj.GetComponent<Personaje>().GetEstadoAlterado();

            switch (numEstado[0]) {
                case 0:
                    estado.color = Color.clear;
                    break;
                case 1:
                    estado.sprite = (Sprite)Resources.Load("Sprites/Interfaz/Estado_Ceguera", typeof(Sprite));
                    estado.color = Color.white;
                    break;
                case 2:
                    estado.sprite = (Sprite)Resources.Load("Sprites/Interfaz/Estado_Hemorragia", typeof(Sprite));
                    estado.color = Color.white;
                    break;
                case 3:
                    estado.sprite = (Sprite)Resources.Load("Sprites/Interfaz/Estado_Envenenado", typeof(Sprite));
                    estado.color = Color.white;
                    break;
                case 5:
                    estado.sprite = (Sprite)Resources.Load("Sprites/Interfaz/Estado_Confusion", typeof(Sprite));
                    estado.color = Color.white;
                    break;
            }

            switch (numEstado[1]) {
                case 0:
                    estado2.color = Color.clear;
                    break;
                case 1:
                    estado2.sprite = (Sprite)Resources.Load("Sprites/Interfaz/Estado_Ceguera", typeof(Sprite));
                    estado2.color = Color.white;
                    break;
                case 2:
                    estado2.sprite = (Sprite)Resources.Load("Sprites/Interfaz/Estado_Hemorragia", typeof(Sprite));
                    estado2.color = Color.white;
                    break;
                case 3:
                    estado2.sprite = (Sprite)Resources.Load("Sprites/Interfaz/Estado_Envenenado", typeof(Sprite));
                    estado2.color = Color.white;
                    break;
                case 5:
                    estado2.sprite = (Sprite)Resources.Load("Sprites/Interfaz/Estado_Confusion", typeof(Sprite));
                    estado2.color = Color.white;
                    break;
            }

        } else {
            panelDatosPersonaje.SetActive(false);
        }

    }

    public void EntreTurno(bool turno) {
        txtTurno.text = GetJugador(turno) + " turn";
        txtTurno.gameObject.SetActive(true);
        Invoke("QuitarTxtTurno", 2f);
    }

    public void QuitarTxtTurno() {
        txtTurno.gameObject.SetActive(false);
    }

    public void OcultarPanelDatos() {
        panelDatosPersonaje.SetActive(false);
    }

    public void FinPartida(bool turno) {
        finPartida.text = "Winner: " + GetJugador(turno); //Mostramos el ganador.
        panelFinPartida.SetActive(true);
    }

    public void AbandonarPartidaPausa(bool turno) {
        abandonado.text = GetJugador(turno) + " surrendered";
        abandonado.gameObject.SetActive(true);
    }

    public string GetJugador(bool turno) {
        if (turno == true) {//Jugador 1
            return nombreJugador1;
        } else { //Jugador 2
            return nombreJugador2;
        }
    }
}
