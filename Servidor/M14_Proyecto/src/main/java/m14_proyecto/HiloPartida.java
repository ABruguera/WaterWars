/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m14_proyecto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Kevin
 */
public class HiloPartida extends Thread {

    private Socket jugador1;
    private Socket jugador2;
    private Servidor servidor;
    private InputStream inJug1;
    private InputStream inJug2;
    private OutputStream outJug1;
    private OutputStream outJug2;

    public HiloPartida(Socket jugador1, Socket jugador2, Servidor servidor) {
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
        this.servidor = servidor;
        try {
            inJug1 = jugador1.getInputStream();
            inJug2 = jugador2.getInputStream();
            outJug1 = jugador1.getOutputStream();
            outJug2 = jugador2.getOutputStream();
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("SE HA INICIADO LA PARTIDA MULTIJUGADOR!!!!");
        try {
            mandarNombreUsuario();
            mandarEquiposSeleccionados();
            iniciarPartida();
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // Método para continuar ejecutando el código.
    private void comprobador(int jugador) {
        Scanner in = null;
        if (jugador == 1) {
            in = new Scanner(inJug1);
        } else if (jugador == 2) {
            in = new Scanner(inJug2);
        }
        in.nextLine();
    }

    // Método para mandar los nombres de usuario a los jugadores.
    private void mandarNombreUsuario() throws IOException {
        // Coge los nombres de usuario de los jugadores.
        String j1 = servidor.cogerUsuarioConectado(jugador1.getInetAddress().toString()).getNombreUsuario();
        String j2 = servidor.cogerUsuarioConectado(jugador2.getInetAddress().toString()).getNombreUsuario();
        String resultado = j1 + "\n" + j2 + "\n";

        // Envia los nombres de usuario a los jugadores
        outJug1.write(resultado.getBytes());
        comprobador(1);
        outJug2.write(resultado.getBytes());
        comprobador(2);
    }

    // Método para mandar los equipos seleccionados a los jugadores.
    private void mandarEquiposSeleccionados() throws IOException {
        // Coge los equipos de los jugadores.
        int j1 = servidor.cogerUsuarioConectado(jugador1.getInetAddress().toString()).getEquipo();
        int j2 = servidor.cogerUsuarioConectado(jugador2.getInetAddress().toString()).getEquipo();
        String resultado = j1 + ":" + j2;

        // Envia los nombres de usuario a los jugadores
        outJug1.write(resultado.getBytes());
        //comprobador(1);
        outJug2.write(resultado.getBytes());
        //comprobador(2);
    }

    // Método para iniciar la partida y controlar los turnos de los jugadores.
    private void iniciarPartida() {
        boolean turnoJugador1 = true;
        boolean finPartida = false;
        while (!finPartida) {
            try {
                if (turnoJugador1) {
                    outJug2.write(recibirInstruccion(1).getBytes());
                    //outJug2.write(new Scanner(inJug1).nextLine().getBytes());
                    //inJug1.reset();
                    //System.out.println(new Scanner(inJug1).nextLine());
                } else {
                    outJug1.write(recibirInstruccion(2).getBytes());
                }
            } catch (IOException e) {
                System.out.println("ERROR: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
    // Método para recibir la instrucción del jugador y retornarlas al contrincante.
    private String recibirInstruccion(int jugador) {
        Scanner in = null;
        if (jugador == 1) {
            in = new Scanner(inJug1);
        } else if (jugador == 2) {
            in = new Scanner(inJug2);
        }
        return in.nextLine();
    }
}
