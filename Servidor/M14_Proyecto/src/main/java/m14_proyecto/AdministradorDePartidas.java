/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m14_proyecto;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Kevin
 */
public class AdministradorDePartidas extends Thread {
    
    private static int PUERTO = 6666;
    private Servidor servidor;
    
    public AdministradorDePartidas(Servidor servidor) {
        this.servidor = servidor;
    }
    
    @Override
    public void run() {
        iniciar();
    }
    
    private void iniciar() {
        ServerSocket servicio = null;
        Socket jugador1 = null;
        Socket jugador2 = null;
        try {
            servicio = new ServerSocket(PUERTO);  // Crear socket servidor.
        } catch (IOException e) {
            System.out.println(e);
        }
        try {
            System.out.println("Administrador de partidas> Escuchando en el puerto: " + PUERTO);
            while (true) {
                jugador1 = servicio.accept();
                System.out.println("Bienvenido jugador 1");
                jugador2 = servicio.accept();
                System.out.println("Bienvenido jugador 2");
                new HiloPartida(jugador1, jugador2, servidor).start();
                jugador1 = null;
                jugador2 = null;
            }
        } catch (IOException e) {
            System.out.println(e);
            System.exit(-1);
        }
    }
}
