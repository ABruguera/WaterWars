/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m14_proyecto;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;

/**
 *
 * @author Kevin
 */
public class DAOBaseDatos {
    
    private Connection conn;
    
    public DAOBaseDatos() {
        conn = null;
    }
    
    // Establece la conexión a la base de datos.
    private void establecerConexion() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://192.168.12.41:3306/BaseDatosServidor?noAccessToProcedureBodies=true", "programador", "PORMD18_akmp");
    }
    
    // Cierra la conexión a la base de datos.
    private void finalizarConexion() throws SQLException {
        conn.close();
    }
    
    // Método para comprobar si el usuario que se quiere registrar ya existe o no en la base de datos.
    public boolean comprobarUsuario(String usuario) {
        boolean existeUsuario = true;
        try {
            establecerConexion();
            CallableStatement cstmtFSelect = conn.prepareCall("{?=call comprobarUsuario(?)}");
            cstmtFSelect.registerOutParameter(1, Types.INTEGER);
            cstmtFSelect.setString(2, usuario);
            cstmtFSelect.execute();
            if (cstmtFSelect.getInt(1) != 0) {
                existeUsuario = false;  // Hay un usuario introducido.
            }
            finalizarConexion();
        } catch (SQLException e) {
            existeUsuario = false;
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
        return existeUsuario;
    }
    
    // Método para registrar el nuevo usuario a la base de datos.
    public boolean registrarUsuario(String usuario, String contrasenya, String nombre, String apellidos, String email, String telefono) {
        boolean comprobacion = false;
        try {
            establecerConexion();
            CallableStatement cstmtFUpdate = conn.prepareCall("{call registrarUsuario(?, ?, ?, ?, ?, ?)}");
            cstmtFUpdate.setString(1, usuario);
            cstmtFUpdate.setString(2, contrasenya);
            cstmtFUpdate.setString(3, nombre);
            cstmtFUpdate.setString(4, apellidos);
            cstmtFUpdate.setString(5, email);
            cstmtFUpdate.setString(6, telefono);
            cstmtFUpdate.execute();
            finalizarConexion();
            comprobacion = true;
        } catch (SQLException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
        return comprobacion;
    }
    
    // Método para comprobar el usuario y contraseña para permitir iniciar sesión o no.
    public boolean comprobarUsuarioContrasenya(String usuario, String contrasenya) {
        boolean usuarioErroneo = true;
        try {
            establecerConexion();
            CallableStatement cstmtFSelect = conn.prepareCall("{?=call comprobarUsuarioContrasenya(?, ?)}");
            cstmtFSelect.registerOutParameter(1, Types.INTEGER);
            cstmtFSelect.setString(2, usuario);
            cstmtFSelect.setString(3, contrasenya);
            cstmtFSelect.execute();
            if (cstmtFSelect.getInt(1) == 1) {
                usuarioErroneo = false;  // Hay un usuario introducido.
            }
            finalizarConexion();
        } catch (SQLException e) {
            usuarioErroneo = true;
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
        return usuarioErroneo;
    }
    
    // Método para obtener el equipo del jugador que se indique.
    public int obtenerEquipoJugador(String usuario) {
        int resultado = -1;
        try {
            establecerConexion();
            CallableStatement cstmtFSelect = conn.prepareCall("{?=call obtenerEquipoJugador(?)}");
            cstmtFSelect.registerOutParameter(1, Types.INTEGER);
            cstmtFSelect.setString(2, usuario);
            cstmtFSelect.execute();
            resultado = cstmtFSelect.getInt(1);
            finalizarConexion();
        } catch (SQLException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
        return resultado;
    }
    
    // Método para cambiar el equipo por defecto en la base de datos.
    public void cambiarEquipo(String usuario, int equipo) {
        try {
            establecerConexion();
            CallableStatement cstmtFUpdate = conn.prepareCall("{call cambiarEquipo(?, ?)}");
            cstmtFUpdate.setString(1, usuario);
            cstmtFUpdate.setInt(2, equipo);
            cstmtFUpdate.execute();
            finalizarConexion();
        } catch (SQLException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
