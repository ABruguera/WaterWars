/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m14_proyecto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Kevin
 */
public class HiloServidor extends Thread {

    private Socket socketServicio;
    private InputStream inputStream;
    private OutputStream outputStream;
    private DAOBaseDatos bd;
    private Servidor servidor;
    private boolean sesionIniciada;
    private boolean salirSinIniciarSesion;

    private String nombreUsuario;
    private int equipo;

    public HiloServidor(Socket socketServicio, DAOBaseDatos bd, Servidor servidor) {
        this.socketServicio = socketServicio;
        this.bd = bd;
        try {
            inputStream = socketServicio.getInputStream();
            outputStream = socketServicio.getOutputStream();
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
        this.servidor = servidor;
        nombreUsuario = "";
        sesionIniciada = false;
        salirSinIniciarSesion = false;
        equipo = 1;
    }

    @Override
    public void run() {
        while (!sesionIniciada && !salirSinIniciarSesion) {
            iniciarConexion();  // Hasta que no se identifique no podrá continuar.
        }

        // En caso de que no se haya iniciado sesión se saltará el siguiente paso.
        if (!salirSinIniciarSesion) {
            boolean salir = false;
            while (!salir) {
                Scanner in = new Scanner(inputStream);
                String[] lectura = in.nextLine().split(":");
                byte opcion = Byte.parseByte(lectura[0]);
                switch (opcion) {
                    case 0:
                        servidor.finalizarSesion(socketServicio.getInetAddress().toString());
                        salir = true;
                        break;
                    case 1:
                        cambiarEquipoJugador(Integer.parseInt(lectura[1]));
                        break;
                    case 2:
                        cargarDatosJugador();
                        break;
                }
            }
        }

        // Cierra las conexiones y cierra el hilo.
        try {
            inputStream.close();
            outputStream.close();
            socketServicio.close();
            System.out.println("El usuario '" + nombreUsuario + "' ha cerrado la sesión.");
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // Método para cambiar el equipo por defecto del jugador.
    private void cambiarEquipoJugador(int equipo) {
        this.equipo = equipo;
        synchronized (bd) {
            bd.cambiarEquipo(nombreUsuario, equipo);
        }
    }

    // Método para enviar los datos del jugador al mismo.
    private void cargarDatosJugador() {
        try {
            outputStream.write((nombreUsuario + ":" + equipo + "\n").getBytes());
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // Método para iniciar sesión o registrarse en el servidor.
    private void iniciarConexion() {
        Scanner in = new Scanner(inputStream);
        byte opcion = Byte.parseByte(in.nextLine());
        switch (opcion) {
            case 0:
                identificarseEnServidor();
                break;
            case 1:
                registrarseEnServidor();
                break;
            case 2:
                salirSinIniciarSesion = true;
                break;
        }
    }

    // Método para identificarse en el servidor.
    private void identificarseEnServidor() {
        try {
            outputStream.write("0".getBytes());  // Manda notificación al cliente para que proceda.
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
        Scanner in = new Scanner(inputStream);
        String res = in.nextLine();
        String[] perfil = res.split(":");
        String usuario = perfil[0];
        String contrasenya = perfil[1];

        String mensaje = "";
        boolean usuarioErroneo = true;
        synchronized (bd) {
            usuarioErroneo = bd.comprobarUsuario(usuario);  // Comprueba que el usuario no exista.
        }
        if (!usuarioErroneo) {
            synchronized (bd) {
                usuarioErroneo = bd.comprobarUsuarioContrasenya(usuario, contrasenya);
            }
            if (!usuarioErroneo) {
                mensaje = "0\n";
                nombreUsuario = usuario;
                sesionIniciada = true;
                synchronized (bd) {
                    equipo = bd.obtenerEquipoJugador(nombreUsuario);
                }
                servidor.identificarUsuario(socketServicio.getInetAddress().toString(), this);
            } else {
                mensaje = "1\n";
            }
        } else {
            mensaje = "2\n";
        }

        try {
            outputStream.write(mensaje.getBytes());
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // Método para registrarse en el servidor.
    private void registrarseEnServidor() {
        try {
            outputStream.write("0".getBytes());  // Manda notificación al cliente para que proceda.
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
        Scanner in = new Scanner(inputStream);
        String res = in.nextLine();
        String[] perfil = res.split(":");
        String usuario = perfil[0];
        String contrasenya = perfil[1];
        String nombre = perfil[2];
        String apellidos = perfil[3];
        String email = perfil[4];
        String telefono = perfil[5];

        String mensaje = "";
        boolean continuarRegistro = false;
        synchronized (bd) {
            continuarRegistro = bd.comprobarUsuario(usuario);  // Comprueba que el usuario no exista.
        }
        if (continuarRegistro) {
            synchronized (bd) {
                continuarRegistro = bd.registrarUsuario(usuario, contrasenya, nombre, apellidos, email, telefono);
            }
            if (continuarRegistro) {
                mensaje = "Register complete.\nYou can login.\n";
            } else {
                mensaje = "Register failed.\n";
            }
        } else {
            mensaje = "The user '" + usuario + "' already exist.\n";
        }

        try {
            outputStream.write(mensaje.getBytes());
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // Método para obtener el nombre de usuario.
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    // Método para obtener el equipo del usuario.
    public int getEquipo() {
        return equipo;
    }
}
