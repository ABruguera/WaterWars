/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m14_proyecto;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Kevin
 */
public class Servidor {
    
    private static int PUERTO = 6669;
    private DAOBaseDatos bd;
    private AdministradorDePartidas adp;
    private Map<String, HiloServidor> usuariosConectados;
    
    public Servidor() {
        bd = new DAOBaseDatos();
        adp = new AdministradorDePartidas(this);
        adp.start();
        usuariosConectados = new HashMap();
    }
    
    public void identificarUsuario(String ip, HiloServidor hs) {
        usuariosConectados.put(ip, hs);
    }
    
    public HiloServidor cogerUsuarioConectado(String ip) {
        return usuariosConectados.get(ip);
    }
    
    public void finalizarSesion(String ip) {
        usuariosConectados.remove(ip);
    }
    
    private void iniciar() {
        ServerSocket servicio = null;
        Socket socketServicio = null;
        try {
            servicio = new ServerSocket(PUERTO);  // Crear socket servidor.
        } catch (IOException e) {
            System.out.println(e);
        }
        try {
            System.out.println("Servidor> Escuchando en el puerto: " + PUERTO);
            while (true) {
                socketServicio = servicio.accept();  // Aceptar un cliente.
                new HiloServidor(socketServicio, bd, this).start();
                        // Crea un hilo para el cliente y lo ejecuta.
                socketServicio = null;  // Ponemos "null" al socketServicio para aceptar más clientes.
            }
        } catch (IOException e) {
            System.out.println(e);
            System.exit(-1);
        }
    }
    
    public static void main(String[] args) {
        Servidor sv = new Servidor();
        sv.iniciar();
    }
}
