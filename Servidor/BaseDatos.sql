DROP DATABASE IF EXISTS BaseDatosServidor;
CREATE DATABASE BaseDatosServidor;
GRANT ALL PRIVILEGES ON BaseDatosServidor.* TO 'admin'@'%' IDENTIFIED BY 'AMN18_akmp' WITH GRANT OPTION;
USE BaseDatosServidor;

/* ======================================================================= */
CREATE TABLE USUARIO (
	idUsuario	INT PRIMARY KEY AUTO_INCREMENT,
	usuario		VARCHAR(20),
	contrasenya	VARCHAR(32),
	nombre		VARCHAR(40),
	apellidos	VARCHAR(40),
	email		VARCHAR(40),
	telefono	VARCHAR(20),
	equipo		INT,
	idTienda	INT
) ENGINE=INNODB;

CREATE TABLE TARJETA_CREDITO (
	idTarjetaCredito	INT PRIMARY KEY AUTO_INCREMENT,
	numero				INT,
	caducidad			VARCHAR(5),
	codigoSeguridad		INT(3),
	idUsuario			INT
) ENGINE=INNODB;

CREATE TABLE ROL (
	idRol		INT PRIMARY KEY,
	nombre		VARCHAR(30),
	permisos	VARCHAR(100)
) ENGINE=INNODB;

CREATE TABLE TIENE (
	usuario		INT,
	rol			INT,
	PRIMARY KEY (usuario, rol)
) ENGINE=INNODB;

CREATE TABLE TIENDA (
	idTienda		INT PRIMARY KEY
) ENGINE=INNODB;

CREATE TABLE PRODUCTO (
	idProducto		INT PRIMARY KEY AUTO_INCREMENT,
	nombre			VARCHAR(50),
	descripcion		VARCHAR(200),
	precio			DOUBLE,
	idTienda		INT
) ENGINE=INNODB;

/* ======================================================================= */
ALTER TABLE USUARIO ADD CONSTRAINT fk_USUARIO_idTienda FOREIGN KEY (idTienda) REFERENCES TIENDA(idTienda);
ALTER TABLE TARJETA_CREDITO ADD CONSTRAINT fk_TARJETA_CREDITO_idUsuario FOREIGN KEY (idUsuario) REFERENCES USUARIO(idUsuario);
ALTER TABLE TIENE ADD CONSTRAINT fk_TIENE_usuario FOREIGN KEY (usuario) REFERENCES USUARIO(idUsuario);
ALTER TABLE TIENE ADD CONSTRAINT fk_TIENE_rol FOREIGN KEY (rol) REFERENCES ROL(idRol);
ALTER TABLE PRODUCTO ADD CONSTRAINT fk_PRODUCTO_idTienda FOREIGN KEY (idTienda) REFERENCES TIENDA(idTienda);

/* ======================================================================= */
DELIMITER //
CREATE PROCEDURE registrarUsuario (usuarioN VARCHAR(20), contrasenyaN VARCHAR(32), nombreN VARCHAR(40), apellidosN VARCHAR(40), emailN VARCHAR(40), telefonoN VARCHAR(20))
BEGIN
	DECLARE id INT;
	INSERT INTO USUARIO (usuario, contrasenya, nombre, apellidos, email, telefono, equipo) VALUES (usuarioN, contrasenyaN, nombreN, apellidosN, emailN, telefonoN, 1);
	SELECT COUNT(*) FROM USUARIO INTO id;
	INSERT INTO TIENE (usuario, rol) VALUES (id, 2);
END;
//
DELIMITER ;

DELIMITER //
CREATE FUNCTION comprobarUsuario (usuarioN VARCHAR(20)) RETURNS INT
BEGIN
	DECLARE resultado INT;
	SELECT COUNT(*) FROM USUARIO WHERE usuario = usuarioN INTO resultado;
	RETURN resultado;
END;
//
DELIMITER ;

DELIMITER //
CREATE FUNCTION comprobarUsuarioContrasenya (usuarioN VARCHAR(20), contrasenyaN VARCHAR(32)) RETURNS INT
BEGIN
	DECLARE resultado INT;
	SELECT COUNT(*) FROM USUARIO WHERE usuario = usuarioN AND contrasenya = contrasenyaN INTO resultado;
	RETURN resultado;
END;
//
DELIMITER ;

DELIMITER //
CREATE FUNCTION obtenerEquipoJugador (usuarioN VARCHAR(20)) RETURNS INT
BEGIN
	DECLARE resultado INT;
	SELECT equipo FROM USUARIO WHERE usuario = usuarioN INTO resultado;
	RETURN resultado;
END;
//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE cambiarEquipo (usuarioN VARCHAR(20), equipoN INT)
BEGIN
	UPDATE USUARIO SET equipo = equipoN WHERE usuario = usuarioN;
END;
//
DELIMITER ;

/* ======================================================================= */
INSERT INTO TIENDA (idTienda) VALUES (1);

INSERT INTO PRODUCTO (nombre, descripcion, precio, idTienda) VALUES ('Skin caballito verde', 'Pinta a tu equipo de caballitos de mar de color verde.', 0.49, 1);
INSERT INTO PRODUCTO (nombre, descripcion, precio, idTienda) VALUES ('Skin pulpo azul', 'Pinta a tu equipo de pulpos de color azul.', 0.49, 1);
INSERT INTO PRODUCTO (nombre, descripcion, precio, idTienda) VALUES ('Skin delfines rojos', 'Pinta a tu equipo de delfines de color rojo.', 0.49, 1);
INSERT INTO PRODUCTO (nombre, descripcion, precio, idTienda) VALUES ('Skin sireno rosa', 'Pinta a tu equipo de sirenos de color rosa.', 0.49, 1);

INSERT INTO USUARIO (usuario, contrasenya, nombre, apellidos, email, telefono, equipo, idTienda) VALUES ('zombie', MD5('1234'), 'Kevin', 'Gómez Codina', 'kevin.gomez.codina@gmail.com', '600564452', 2, 1);
INSERT INTO USUARIO (usuario, contrasenya, nombre, apellidos, email, telefono, equipo, idTienda) VALUES ('pintador', MD5('1234'), 'Miguel', 'Hernández Martín', 'miguel@gmail.com', '694542386', 3, 1);
INSERT INTO USUARIO (usuario, contrasenya, nombre, apellidos, email, telefono, equipo, idTienda) VALUES ('paco', MD5('1234'), 'Pau', 'López Gálvez', 'argoUML@gmail.com', '500385678', 1, 1);
INSERT INTO USUARIO (usuario, contrasenya, nombre, apellidos, email, telefono, equipo, idTienda) VALUES ('ElViejo', MD5('1234'), 'Abraham', 'Bruguera Calvo', 'viejo_calvo@gmail.com', '693843471', 4, 1);

INSERT INTO ROL (idRol, nombre, permisos) VALUES (1, 'Administrador', 'Tiene control total del servidor.');
INSERT INTO ROL (idRol, nombre, permisos) VALUES (2, 'Usuario estándar', 'Solamente puede jugar en partidas online con otros usuarios y modificar su perfil.');

INSERT INTO TIENE (usuario, rol) VALUES (1, 1);
INSERT INTO TIENE (usuario, rol) VALUES (2, 1);
INSERT INTO TIENE (usuario, rol) VALUES (3, 1);
INSERT INTO TIENE (usuario, rol) VALUES (4, 1);

SHOW TABLES;
SELECT * FROM USUARIO;
SELECT * FROM TARJETA_CREDITO;
SELECT * FROM ROL;
SELECT * FROM TIENE;
SELECT * FROM TIENDA;
SELECT * FROM PRODUCTO;

/* ======================================================================= */
DROP USER 'programador'@'%';
CREATE USER 'programador'@'%' IDENTIFIED BY 'PORMD18_akmp';
GRANT SELECT
ON BaseDatosServidor.USUARIO TO 'programador'@'%' WITH GRANT OPTION;
GRANT SELECT
ON BaseDatosServidor.ROL TO 'programador'@'%' WITH GRANT OPTION;
GRANT SELECT
ON BaseDatosServidor.TIENE TO 'programador'@'%' WITH GRANT OPTION;
GRANT EXECUTE ON BaseDatosServidor.* TO 'programador'@'%' WITH GRANT OPTION;